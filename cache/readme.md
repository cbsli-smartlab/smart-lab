# The Smart Laboratory Project
## Project Members
* Kevin Chen
* Kunye Chen
* Binghan He
* Melissa Lee
* Lijia Liu
* Rachel Schlossman

## Project Definition
The Smart Laboratory project is a continuation of the Could-Based Shared Lab Initiative, which seeks to provide high-quality laboratory experience to distance-learning students. Our group will be implementing concepts covered in the Decision and Control of Humanoid Robotics (ME 396D) course at the University of Texas at Austin to synthesize a controller that provides a safe and high-quality learning experience.
