from scipy import integrate 
from scipy import signal
from disc import disc
import numpy as np
import matplotlib.pyplot as plt
import time

sim = disc()

# plant dynamics (m)
m_k = 360. # motor inertia (kg)
b_eff = 2200. # coeffient of friction (N*s/m)
k_s = 350000. # spring constant (N/m)
beta = 219. # drivetrain coefficient (N/A)
zm, pm, km = signal.tf2zpk(np.array([beta * k_s]), np.array([m_k, b_eff, k_s]))
Am, Bm, Cm, Dm = sim.zpk2ss(zm, pm, km)
# print Am, Bm, Cm, Dm

# feedback force controller (c)
zc, pc, kc = signal.tf2zpk(np.array([0.0008811, 0.05]), np.array([1.]))
Ac, Bc, Cc, Dc = sim.zpk2ss(zc, pc, kc)

# feedforward force controller (f)
tfc = sim.lti2sym(signal.lti(np.array([0.0008811, 0.05]), np.array([1.])))
tfo = sim.lti2sym(signal.lti(np.array([1.]), np.array([beta])))
tf  = sim.sym2lti((tfc + tfo) / tfc)
zf, pf, kf = signal.tf2zpk(tf.num, tf.den)
Af, Bf, Cf, Df = sim.zpk2ss(zf, pf, kf)

# time delay approximation (d)
zd, pd, kd = sim.pade(0.)
Ad, Bd, Cd, Dd = sim.zpk2ss(zd, pd, kd)

# combined dynamics (for prediction)
A, B, C, D = sim.cmb(Ac, Bc, Cc, Dc, Am, Bm, Cm, Dm)
A, B, C, D = sim.cmb(A,  B,  C,  D,  Ad, Bd, Cd, Dd)
# A, B, C, D = sim.fb (A,  B,  C,  D)

# initialization
xf = np.zeros((Af.shape[0], 1))
xc = np.zeros((Ac.shape[0], 1))
xm = np.zeros((Am.shape[0], 1))
xd = np.zeros((Ad.shape[0], 1))
x  = np.append(xc, xm, 0)
x  = np.append(x,  xd, 0)
Y = []
U = []
T = [] # time sequence

R = signal.chirp(sim.T, sim.fmin, sim.tf, sim.fmax)
# R = np.ones(R.size)
# R = signal.square(np.pi * sim.T)

i = 0;

for t in sim.T:

	r  = np.array([[R[i]]])

	uf = r 
	yf, xf = sim.intg(xf, uf, Af, Bf, Cf, Df) # feedforward force controller

	x  = np.append(xc, xm, 0)
	x  = np.append(x,  xd, 0)
	yd = sim.pdt(x, yf, C, D, 'closed') # feedback ouput prediction
	
	uc = yf - yd
	yc, xc = sim.intg(xc, uc, Ac, Bc, Cc, Dc) # feedback force controller 

	um = r 
	ym, xm = sim.intg(xm, um, Am, Bm, Cm, Dm) # plant dynamics (replaced by actuator)

	ud = ym
	yd, xd = sim.intg(xd, ud, Ad, Bd, Cd, Dd) # time delay approximation

	# um  = yf
	# yd, x  = sim.intg(x, um, A, B, C, D)

	print 't = ', t

	Y.extend([yd])
	U.extend([yf])
	T.extend([t])

	i = i + 1

Y = np.array(Y)
Y.resize(Y.shape[0])
U = np.array(U)
U.resize(U.shape[0])
T = np.array(T)
T.resize(T.shape[0])

# show plots
sim.timePlot(T, Y, R, 'splt') # time domain plots 
sim.freqPlot(Y, R) # frequency domain plots 
plt.show()