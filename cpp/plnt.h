// ---------------
// plnt.h
// ---------------

#ifndef plnt_h
#define plnt_h

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/math/tools/polynomial.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <math.h>

#define pi 3.14159265
using namespace boost::numeric::ublas;
using namespace boost::math::tools; // for polynomial  

void intg (matrix<double>& x, const matrix<double>& u, matrix<double>& y, const matrix<double>& A, const matrix<double>& B, const matrix<double>& C, const matrix<double>& D) {
    y = prod (C, x) + prod (D, u);
    x = prod (A, x) + prod (B, u);
}

polynomial<double> v2p (const vector<double>& v) {
    polynomial<double> p (v.rbegin(), v.rend());
    return p;
}

vector<double> p2v (const polynomial<double>& p) {
    const int n = p.size();
    vector<double> v (n);
    for (int i = 0; i < n; i++) {
        v (i) = p [n - 1 - i];
    }
    return v;
}

class tf {

private:

public:
    polynomial<double> b;
    polynomial<double> a;
    vector<double> num;
    vector<double> den;

    tf (const vector<double>& _num, const vector<double>& _den) : num(_num), den(_den) {
        b = v2p (num);
        a = v2p (den);
    };

    tf (const polynomial<double>& _b, const polynomial<double>& _a) : b(_b), a(_a) {
        num = p2v (b);
        den = p2v (a);
    };

    bool operator == (tf rhs) const {
        return (b == rhs.b) && (a == rhs.a);
    };
};

inline tf operator + (const tf& tf1, const tf& tf2) {
    if (tf1.a == tf2.a) {
        polynomial<double> _b = tf1.b + tf2.b;
        polynomial<double> _a = tf1.a;
        return tf (_b, _a);
    }
    else {
        polynomial<double> _b = tf1.b * tf2.a + tf1.a * tf2.b;
        polynomial<double> _a = tf1.a * tf2.a;
        return tf (_b, _a);
    }   
}

inline tf operator - (const tf& tf1, const tf& tf2) {
    if (tf1.a == tf2.a) {
        polynomial<double> _b = tf1.b - tf2.b;
        polynomial<double> _a = tf1.a;
        return tf (_b, _a);
    }
    else {
        polynomial<double> _b = tf1.b * tf2.a - tf1.a * tf2.b;
        polynomial<double> _a = tf1.a * tf2.a;
        return tf (_b, _a);
    }  
}

inline tf operator * (const tf& tf1, const tf& tf2) {
    polynomial<double> _b = tf1.b * tf2.b;
    polynomial<double> _a = tf1.a * tf2.a;
    return tf (_b, _a);
}

inline tf operator / (const tf& tf1, const tf& tf2) {
    polynomial<double> _b = tf1.b * tf2.a;
    polynomial<double> _a = tf1.a * tf2.b;
    return tf (_b, _a);
}

tf pade (const double& T) {
    vector<double> num (4);
    num (0) = - pow (T, 3.) / 120.;
    num (1) =   pow (T, 2.) /  10.;
    num (2) = - T / 2.;
    num (3) =   1.;
    vector<double> den (4);
    den (0) =   pow (T, 3.) / 120.;
    den (1) =   pow (T, 2.) /  10.;
    den (2) =   T / 2.;
    den (3) =   1.;
    return tf (num, den);
}

tf lpf (const double& fc) {
    vector<double> num (1);
    num (0) = 1.;
    vector<double> den (3);
    den (0) = pow((1. / (2 * pi * fc)), 2);
    den (1) = sqrt(2.) * 1. / (2 * pi * fc);
    den (2) = 1.;
    return tf (num, den);
}

tf prop (tf& tfc, const double& T) {
    vector<double> b = tfc.num;
    vector<double> a = tfc.den;
    const polynomial<double> p1 {{- 1., 1.}};
    const polynomial<double> p2 {{1., 1.}};
    polynomial<double> n;
    polynomial<double> d;
    int na = a.size();
    int nb = b.size();

    for (int i = 0; i < na; i++) {
        d += a (i) * pow ((2. / T), na - 1 - i) * pow (p1, na - 1 - i) * pow (p2, i);
        // std::cout << d << std::endl;
    };

    for (int i = 0; i < nb; i++) {
        n += b (i) * pow ((2. / T), nb - 1 - i) * pow (p1, nb - 1 - i) * pow (p2, i + (na - nb));
        // std::cout << n << std::endl;
    };

    b = vector<double> (na);
    a = vector<double> (na);
    for (int i = 0; i < na; i++) {
        b (i) = n [na - 1 - i] / d [na - 1];
        a (i) = d [na - 1 - i] / d [na - 1];
    }
    return tf (b, a);
}

tf c2d (tf& tfc, const double& T) {
    if (tfc.num.size() <= tfc.den.size()) {
        tf tfd = prop (tfc, T);
        return tfd;
    }
    else {
        tf tfd = tf (tfc.den, tfc.num);
        tfd = prop (tfd, T);
        tfd = tf (tfd.den, tfd.num);
        return tfd;
    }
}

void tf2ss (const tf& tfd, matrix<double>& A, matrix<double>& B, matrix<double>& C, matrix<double>& D) {   
    vector<double> num = tfd.num;
    vector<double> den = tfd.den;

    int nn = num.size();
    int nd = den.size();
    vector<double> a (nd);
    vector<double> b (nd);
    for (int i = 0; i < nd; i++) {
        if (nd - i > nn) {
            b (i) = 0.;
        }
        else {
            b (i) = num (i - (nd - nn)) / den (0);
        };
        a (i) = den (i) / den (0);
        // std::cout << i << std::endl;
    };

    A = zero_matrix<double> (nd - 1, nd - 1);
    B = zero_matrix<double> (nd - 1, 1);
    C = zero_matrix<double> (1, nd - 1);
    D = zero_matrix<double> (1, 1);

    for (int i = 0; i < nd - 1; i++) {
        if (i < nd - 2) {
            A (i + 1, i) = 1;
        };
        A (0, i) = - a (i + 1);        
        C (0, i) = b (i + 1) - a (i + 1) * b (0) ;
    }
    B (0, 0) = 1;
    D (0, 0) = b(0);
}

#endif