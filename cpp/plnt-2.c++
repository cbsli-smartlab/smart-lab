#include <zmq.hpp>
#include <string>
#include <iostream>
#include <sstream>
#ifndef _WIN32
#include <unistd.h>
#else
#include <windows.h>
#define sleep(n)    Sleep(n)
#endif
#include <stdio.h>      /* printf, fgets */
#include <stdlib.h>     /* atof */
#include "plnt.h"

using namespace boost::numeric::ublas;
using namespace boost::math::tools; // for polynomial  

double m_k   = 360.;    // motor inertia (kg)
double b_eff = 2200.;   // coeffient of friction (N*s/m)
double k_s   = 350000.; // spring constant (N/m)
double beta  = 219.;    // drivetrain coefficient (N/A)

double j_a = 0.0164;
double b_a = 0.065;
double b   = 0.025;
double c   = 0.125;
double g   = 9.81;
double m_a = 0.665;
double l_m = 0.157;
double phi = 0.001;
double off  = 3.14159265 / 4.;
double l; 
double tg;
double fs = 1000.;
double r;

int main () {

    vector<double> num1(1); num1 (0) = beta * k_s;
    vector<double> den1(3); den1 (0) = m_k; den1 (1) = b_eff; den1 (2) = k_s;
    matrix<double> Am; matrix<double> Bm; matrix<double> Cm; matrix<double> Dm;
    tf tf1 = tf (num1, den1);
    tf tfd = c2d (tf1, 1./fs);
    tf2ss (tfd, Am, Bm, Cm, Dm);    

    vector<double> num2(1); num2 (0) = 1.;
    vector<double> den2(3); den2 (0) = j_a; den2 (1) = b_a; den2 (2) = 0.;
    matrix<double> Aa; matrix<double> Ba; matrix<double> Ca; matrix<double> Da;
    tf1 = tf (num2, den2);
    tfd = c2d (tf1, 1./fs);
    tf2ss (tfd, Aa, Ba, Ca, Da);

    vector<double> num3; vector<double> den3; 
    tf1 = pade (0./fs);
    matrix<double> Ad; matrix<double> Bd; matrix<double> Cd; matrix<double> Dd;
    tfd = c2d (tf1, 1./fs);
    tf2ss (tfd, Ad, Bd, Cd, Dd);
    // std::cout << Ad << std::endl;
    // std::cout << Bd << std::endl;
    // std::cout << Cd << std::endl;
    // std::cout << Dd << std::endl << std::endl;

    vector<double> num4(2); num4 (0) = 0.0008811; num4 (1) = 0.05;
    vector<double> den4(1); den4 (0) = 1.;
    matrix<double> Ac; matrix<double> Bc; matrix<double> Cc; matrix<double> Dc;
    tf1 = tf (num4, den4);
    tfd = c2d (tf1, 1./fs);
    tf2ss (tfd, Ac, Bc, Cc, Dc);
    // std::cout << Ac << std::endl;
    // std::cout << Bc << std::endl;
    // std::cout << Cc << std::endl;
    // std::cout << Dc << std::endl << std::endl;

    tf1 = tf (num4, den4);
    vector<double> num5(1); num5 (0) = 1.;
    vector<double> den5(1); den5 (0) = beta;
    tf tf2 (num5, den5);
    tf tf3 = (tf1 + tf2) / tf1;
    matrix<double> Af; matrix<double> Bf; matrix<double> Cf; matrix<double> Df;
    tfd = c2d (tf3, 1./fs);    
    tf2ss (tfd, Af, Bf, Cf, Df);
    // std::cout << Af << std::endl;
    // std::cout << Bf << std::endl;
    // std::cout << Cf << std::endl;
    // std::cout << Df << std::endl << std::endl;

    vector<double> num6(3); num6 (0) = j_a; num6 (1) = b_a; num6 (2) = 0.;
    vector<double> den6(1); den6 (0) = 1.;
    tf1 = tf (num6, den6);
    tf2 = lpf (10.);
    tf3 = tf1 * tf2;
    matrix<double> Ap; matrix<double> Bp; matrix<double> Cp; matrix<double> Dp;
    tfd = c2d (tf3, 1./fs);    
    tf2ss (tfd, Ap, Bp, Cp, Dp);
    // std::cout << Ap << std::endl;
    // std::cout << Bp << std::endl;
    // std::cout << Cp << std::endl;
    // std::cout << Dp << std::endl << std::endl;

    tf1 = lpf (35.);
    matrix<double> A2; matrix<double> B2; matrix<double> C2; matrix<double> D2;
    tfd = c2d (tf1, 1./fs);    
    tf2ss (tfd, A2, B2, C2, D2);
    // std::cout << A2 << std::endl;
    // std::cout << B2 << std::endl;
    // std::cout << C2 << std::endl;
    // std::cout << D2 << std::endl << std::endl;

    tf1 = lpf (10.);
    tf2 = lpf (35.);
    tf3 = tf2 / tf1;
    matrix<double> A3; matrix<double> B3; matrix<double> C3; matrix<double> D3;
    tfd = c2d (tf3, 1./fs);    
    tf2ss (tfd, A3, B3, C3, D3);
    // std::cout << A3 << std::endl;
    // std::cout << B3 << std::endl;
    // std::cout << C3 << std::endl;
    // std::cout << D3 << std::endl << std::endl;

    matrix<double> xm = zero_matrix<double> (Am.size1(), 1);
    matrix<double> um = zero_matrix<double> (1, 1);
    matrix<double> ym = zero_matrix<double> (1, 1);

    matrix<double> xa = zero_matrix<double> (Aa.size1(), 1);
    matrix<double> ua = zero_matrix<double> (1, 1);
    matrix<double> ya = zero_matrix<double> (1, 1);

    matrix<double> xd = zero_matrix<double> (Ad.size1(), 1);
    matrix<double> ud = zero_matrix<double> (1, 1);
    matrix<double> yd = zero_matrix<double> (1, 1);

    matrix<double> xc = zero_matrix<double> (Ac.size1(), 1);
    matrix<double> uc = zero_matrix<double> (1, 1);
    matrix<double> yc = zero_matrix<double> (1, 1);

    matrix<double> xf = zero_matrix<double> (Af.size1(), 1);
    matrix<double> uf = zero_matrix<double> (1, 1);
    matrix<double> yf = zero_matrix<double> (1, 1);

    matrix<double> xp = zero_matrix<double> (Ap.size1(), 1);
    matrix<double> up = zero_matrix<double> (1, 1);
    matrix<double> yp = zero_matrix<double> (1, 1);

    matrix<double> x2 = zero_matrix<double> (A2.size1(), 1);
    matrix<double> u2 = zero_matrix<double> (1, 1);
    matrix<double> y2 = zero_matrix<double> (1, 1);

    matrix<double> x3 = zero_matrix<double> (A3.size1(), 1);
    matrix<double> u3 = zero_matrix<double> (1, 1);
    matrix<double> y3 = zero_matrix<double> (1, 1);

    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REP);
    socket.bind ("tcp://*:5555");

    while (true) {
        zmq::message_t request;
        socket.recv (&request);

        // const clock_t begin_time = clock();
        
        l  = (c * b * sin(ya (0, 0) + off)) / sqrt(b * b + c * c - 2 * b * c * cos(ya (0, 0) + off));
        tg = - m_a * g * l_m * cos(ya (0, 0) + off + phi);

        r = atof ((char *) request.data ());
        // std::cout << r << std::endl;

        up (0, 0) = r - (y3 (0, 0) - y2 (0, 0));
        intg (xp, up, yp, Ap, Bp, Cp, Dp);

        uf (0, 0) = (yp (0, 0) + tg) / l;
        intg (xf, uf, yf, Af, Bf, Cf, Df);

        uc (0, 0) = yf (0, 0) - yd (0, 0);
        intg (xc, uc, yc, Ac, Bc, Cc, Dc);        
        
        um (0, 0) = yc (0, 0);
        intg (xm, um, ym, Am, Bm, Cm, Dm);
        // std::cout << ym << std::endl;

        ud (0, 0) = ym (0, 0);
        intg (xd, ud, yd, Ad, Bd, Cd, Dd);
        // std::cout << yd << std::endl;      

        ua (0, 0) = yd (0, 0) * l - tg;
        intg (xa, ua, ya, Aa, Ba, Ca, Da);
        // std::cout << ya << std::endl;

        u3 (0, 0) = ya (0, 0);
        intg (x3, u3, y3, A3, B3, C3, D3);

        u2 (0, 0) = r - (y3 (0, 0) - y2 (0, 0));
        intg (x2, u2, y2, A2, B2, C2, D2);        

        const char fmt [] = "%f %f %f %f %f %f";
        int size = snprintf (0, 0, fmt, xm(0, 0), xm(1, 0), ym (0, 0), xa(0, 0), xa(1, 0), ya (0, 0));
        char * buf = new char [size + 11];
        sprintf (buf, fmt, xm(0, 0), xm(1, 0), ym (0, 0), xa(0, 0), xa(1, 0), ya (0, 0));

        zmq::message_t reply (size);
        memcpy (reply.data (), buf, size);
        socket.send (reply);
        // std::cout << buf << std::endl;
        // std::cout << float( clock () - begin_time ) / CLOCKS_PER_SEC << std::endl;
    }
}