from scipy import signal
import numpy as np
import sympy as sym
import matplotlib.pyplot as plt
import time

class disc(object):
	"""docstring for cont"""
	def __init__(self):
		self.tf = .25 # final time (s)
		self.n1 = 1000. # sampling frequency of simulation (Hz)
		self.fmin = .01 # minimum frequency of chirp signal (Hz)
		self.fmax = 100. # maximum frequency of chirp signal (Hz)
		self.T = np.linspace(0, self.tf, self.tf * self.n1 + 1)

	# discrete dynamics
	def intg(self, x, u, A, B, C, D):	
		y = np.dot(C, x) + np.dot(D, u)
		x = np.dot(A, x) + np.dot(B, u)	
		return y, x

	def cmb(self, A1, B1, C1, D1, A2, B2, C2, D2):
		A = np.append(np.append(A1, np.zeros((A1.shape[0], A2.shape[1])), 1), np.append(np.dot(B2, C1), A2, 1), 0)
		B = np.append(B1, np.dot(B2, D1), 0)
		C = np.append(np.dot(D2, C1), C2, 1)
		D = np.dot(D2, D1)
		return A, B, C, D

	def fb(self, A1, B1, C1, D1):
		A = A1 - np.dot(B1, C1) / (1. + D1) 
		B = B1 / (1. + D1) 
		C = C1 / (1. + D1)
		D = D1 / (1. + D1)
		return A, B, C, D 

	# def pdt(self, xp, xc, xo, r, Cp, Dp, Cc, Dc, Co, Do):	
	# 	y = (np.dot(Cp, xp) + np.dot(Dp, np.dot(Cc, xc) + np.dot(Dc, r) + np.dot(Co, xo) + np.dot(Do, r))) / (1. + np.dot(Dp, Dc))
	# 	return y

	def pdt(self, x, r, C, D, loop):	
		if loop == 'closed':
			y = (np.dot(C, x) + np.dot(D, r)) / (1. + D)
		elif loop == 'open':
			y = np.dot(C, x) + np.dot(D, r)
		return y

	def lpf(self, fc):
		num = np.array([1.])
		den = np.array([(1. / (2 * np.pi * fc)) ** 2, np.sqrt(2.) * 1. / (2 * np.pi * fc), 1.])
		return num, den

	def pade(self, T):
		num = np.array([- T ** 3. / 120., T ** 2. / 10., - T / 2., 1.])
		den = np.array([  T ** 3. / 120., T ** 2. / 10.,   T / 2., 1.])
		z, p, k = signal.tf2zpk(num, den)
		return z, p, k

	def lti2sym(self, lsys, s=sym.Symbol('s'), symplify=True):
	    """ Convert Scipy's LTI instance to Sympy expression """	    
	    G = sym.Poly(lsys.num, s) / sym.Poly(lsys.den, s)
	    return sym.simplify(G) if symplify else G

	def sym2lti(self, xpr, s=sym.Symbol('s')):
	    """ Convert Sympy transfer function polynomial to Scipy LTI """
	    num, den = sym.simplify(xpr).as_numer_denom()  # expressions
	    p_num_den = sym.poly(num, s), sym.poly(den, s)  # polynomials
	    c_num_den = [sym.expand(p).all_coeffs() for p in p_num_den]  # coefficients
	    l_num, l_den = [sym.lambdify((), c)() for c in c_num_den]  # convert to floats
	    return signal.lti(l_num, l_den)

	def zpk2ss(self, z, p, k):
		if p.size is 0 and z.size is 0:
			z = np.array([0.])
			p = np.array([0.])

		if p.size < z.size:
			p0 = z
			z0 = p
			k0 = 1 / k
			z0, p0, k0, dt = signal.cont2discrete((z0, p0, k0), 1/self.n1, 'bilinear')
			p = z0
			z = p0
			k = 1 / k0

		else:
			z, p, k, dt = signal.cont2discrete((z, p, k), 1/self.n1, 'bilinear')

		A, B, C, D = signal.zpk2ss(z, p, k)
		D = np.reshape(D, (D.shape[0], 1))
		return A, B, C, D

	def timePlot(self, T, Y, U, style):

		if style == 'splt':		
			fig = plt.figure("Time Response")

			ax2 = fig.add_subplot(2,1,1)
			plt.plot(T, U, linewidth = 2.0, label='input')
			ax2.set_title('Input',fontsize=20)
			ax2.set_ylabel(r"Motor Current (A)",fontsize=15)

			ax1 = fig.add_subplot(2,1,2)
			plt.plot(T, Y, linewidth = 2.0, label='output')
			ax1.set_title('Output',fontsize=20)
			ax1.set_ylabel(r"Spring Force (N)",fontsize=15)
			ax1.set_xlabel('Time (s)',fontsize=15)

		elif style == 'cmb':
			fig = plt.figure("Time Response")

			ax = fig.add_subplot(1,1,1)
			plt.plot(T, U, linewidth = 2.0, label='desired')
			plt.plot(T, Y, linewidth = 2.0, label='actual')
			plt.legend(loc=4)
			ax.set_title('Time Response',fontsize=20)
			ax.set_ylabel(r"Joint Angle (rad)",fontsize=15)
		return

	def freqPlot(self, Y, U):
		fftY = np.fft.fft(Y)
		fftU = np.fft.fft(U)
		TF = fftY / fftU

		freq = np.linspace(0., self.n1, int(self.tf*self.n1)+1)
		# freq[0] = self.fmin
		start = int(np.shape((abs(TF)))[0] * (self.fmin / self.n1))
		end = int(np.shape((abs(TF)))[0] * (self.fmax / self.n1))
		dB = 20 * np.log10(abs(TF))
		dG = (np.unwrap(np.angle(TF)) / np.pi) * 180

		fig = plt.figure("Frequency Response")

		plt.subplot(211)
		plt.title("Magitude",fontsize=20)
		plt.plot(freq[start:end], dB[start:end])
		plt.ylabel(r"Magnitude (dB)",fontsize=15)
		plt.xscale('log')
		
		plt.subplot(212)
		plt.title("Phase",fontsize=20)
		plt.plot(freq[start:end], dG[start:end])
		plt.ylabel(r"Phase (degree)",fontsize=15)
		plt.xscale('log')
		plt.xlabel("Frequency (Hz)",fontsize=15)
		return