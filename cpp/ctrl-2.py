from scipy import integrate 
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import time
import socket
import pickle
import zmq
from disc import disc

sim = disc()

context = zmq.Context()

#  Socket to talk to serverk
print("Connecting to hello world server")
z = context.socket(zmq.REQ)
z.connect("tcp://localhost:5555")

# plant dynamics (m)
m_k   = 360.    # motor inertia (kg)
b_eff = 2200.   # coeffient of friction (N*s/m)
k_s   = 350000. # spring constant (N/m)
beta  = 219.    # drivetrain coefficient (N/A)
zm, pm, km = signal.tf2zpk(np.array([beta * k_s]), np.array([m_k, b_eff, k_s]))
Am, Bm, Cm, Dm = sim.zpk2ss(zm, pm, km)

# feedback force controller (c)
zc, pc, kc = signal.tf2zpk(np.array([0.0008811, 0.05]), np.array([1.]))
Ac, Bc, Cc, Dc = sim.zpk2ss(zc, pc, kc)
# print Ac, Bc, Cc, Dc
# print ''

# feedforward force controller (f)
tfc = sim.lti2sym(signal.lti(np.array([0.0008811, 0.05]), np.array([1.])))
tfo = sim.lti2sym(signal.lti(np.array([1.]), np.array([beta])))
tf  = sim.sym2lti((tfc + tfo) / tfc)
zf, pf, kf = signal.tf2zpk(tf.num, tf.den)
Af, Bf, Cf, Df = sim.zpk2ss(zf, pf, kf)
# print Af, Bf, Cf, Df
# print ''

# time delay approximation (d)
zd, pd, kd = sim.pade(0.)
Ad, Bd, Cd, Dd = sim.zpk2ss(zd, pd, kd)

# pendulum arm dynamics (a)
j_a = 0.0164
b_a = 0.065
b   = 0.025
c   = 0.125
g   = 9.81
m_a = 0.665
l_m = 0.157
phi = 0.001
za, pa, ka = signal.tf2zpk(np.array([1.]), np.array([j_a, b_a, 0.]))
Aa, Ba, Ca, Da = sim.zpk2ss(za, pa, ka)
# print Aa, Ba, Ca, Da

# feedforward position controller  (p)
tf1 = sim.lti2sym(signal.lti(np.array([j_a, b_a, 0.]), np.array([1.])))
num2, den2 = sim.lpf(10.)
tf2 = sim.lti2sym(signal.lti(num2, den2))
tf  = sim.sym2lti(tf1 * tf2)
zp, pp, kp = signal.tf2zpk(tf.num, tf.den)
Ap, Bp, Cp, Dp = sim.zpk2ss(zp, pp, kp)
# print Ap, Bp, Cp, Dp
# print ''

num2, den2 = sim.lpf(35.)
z2, p2, k2 = signal.tf2zpk(num2, den2)
A2, B2, C2, D2 = sim.zpk2ss(z2, p2, k2)
# print A2, B2, C2, D2
# print ''

num1, den1 = sim.lpf(10.)
tf1 = sim.lti2sym(signal.lti(num1, den1))
num2, den2 = sim.lpf(35.)
tf2 = sim.lti2sym(signal.lti(num2, den2))
tf  = sim.sym2lti(tf2 / tf1)
z3, p3, k3 = signal.tf2zpk(tf.num, tf.den)
A3, B3, C3, D3 = sim.zpk2ss(z3, p3, k3)
# print A3, B3, C3, D3
# print ''

# combined dynamics (for prediction)
A1, B1, C1, D1 = sim.cmb(Ac, Bc, Cc, Dc, Am, Bm, Cm, Dm)
# A1, B1, C1, D1 = sim.cmb(A1, B1, C1, D1, Ad, Bd, Cd, Dd)
# A1, B1, C1, D1 = sim.fb (A1, B1, C1, D1)

# initialization
xm = np.zeros((Am.shape[0], 1))
xf = np.zeros((Af.shape[0], 1))
xc = np.zeros((Ac.shape[0], 1))
xd = np.zeros((Ad.shape[0], 1))
xa = np.zeros((Aa.shape[0], 1))
xp = np.zeros((Ap.shape[0], 1))
ya = np.zeros((Ca.shape[0], 1))
ym = np.zeros((Cm.shape[0], 1))
x1 = np.append(xc, xm, 0)
# x1 = np.append(x1, xd, 0)
x2 = np.zeros((A2.shape[0], 1))
y2 = np.zeros((C2.shape[0], 1))
x3 = np.zeros((A3.shape[0], 1))
y3 = np.zeros((C3.shape[0], 1))
Y = []
U = []
T = [] # time sequence

R = signal.chirp(sim.T, sim.fmin, sim.tf, sim.fmax)
# R = (np.pi / 12) * np.ones(R.size)
R = (np.pi / 12) * signal.square(2. * np.pi * sim.T / 50.) # + np.ones(R.size)

i = 0;

pi = np.pi / 4

for t in sim.T:
	# start_time = time.time()
	# u = R[i]
	
	l  = (c * b * np.sin(ya + pi)) / np.sqrt(b ** 2 + c ** 2 - 2 * b * c * np.cos(ya + pi))
	tg = - m_a * g * l_m * np.cos(ya + pi + phi)

	r  = np.array([[R[i]]])

	# up = r - (y3 - y2)
	# yp, xp = sim.intg(xp, up, Ap, Bp, Cp, Dp) # feedforward force controller

	# uf = (yp + tg) / l 
	# # uf = r  
	# yf, xf = sim.intg(xf, uf, Af, Bf, Cf, Df) # feedforward force controller

	# x1 = np.append(xc, xm, 0)
	# # x1 = np.append(x1, xd, 0)
	# y1 = sim.pdt(x1, yf, C1, D1, 'closed') # feedback ouput prediction
	
	# uc = yf - ym
	# yc, xc = sim.intg(xc, uc, Ac, Bc, Cc, Dc) # feedback force controller 

	inpt = str(r[0, 0])
	# inpt = inpt.encode('utf-8')

	z.send(inpt) 
	data = z.recv()
	# print data 

	data = data.split(' ')
	xm = np.array([[float(data[0])], [float(data[1])]])
	xa = np.array([[float(data[3])], [float(data[4])]])
	ym = float(data[2])
	ya = float(data[5])
	ym = np.array([[ym]])
	ya = np.array([[ya]])

	# u3 = ya
	# y3, x3 = sim.intg(x3, u3, A3, B3, C3, D3)

	# u2 = r - (y3 - y2)
	# y2, x2 = sim.intg(x2, u2, A2, B2, C2, D2)

	Y.extend([ya])
	U.extend([ym])
	T.extend([t])

	# print time.time() - start_time
	# time.sleep(1/sim.n1)
	i = i + 1
	print t

Y = np.array(Y)
Y.resize(Y.shape[0])
U = np.array(U)
U.resize(U.shape[0])
T = np.array(T)
T.resize(T.shape[0])

sim.timePlot(T, Y, R, 'cmb') # time domain plots 
# sim.freqPlot(Y, R) # frequency domain plots 
plt.show()