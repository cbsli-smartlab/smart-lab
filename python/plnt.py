from scipy import integrate 
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import time
import socket
import pickle
import zmq
from cont import cont
from disc import disc


context = zmq.Context()

#  Socket to talk to serverk
print("Connecting to hello world server")
z = context.socket(zmq.REQ)
z.connect("tcp://localhost:5555")

HOST = 'localhost'
PORT = 50007
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(5)
conn, addr = s.accept()

sim = disc()

z1 = np.array([])
p1 = np.array([])
k1 = 1.
A1, B1, C1, D1 = sim.zpk2ss(z1, p1, k1)

m_k = 360. # motor inertia (kg)
b_eff = 2200. # coeffient of friction (N*s/m)
k_s = 350000. # spring constant (N/m)
beta = 219. # drivetrain coefficient (N/A)
z2, p2, k2 = signal.tf2zpk(np.array([beta * k_s]), np.array([m_k, b_eff, k_s]))
A2, B2, C2, D2 = sim.zpk2ss(z2, p2, k2)

x1 = np.zeros((A1.shape[0], 1))
x2 = np.zeros((A2.shape[0], 1))

Y = []
U = []
T = [] # time sequence

i = 0;

u1 = np.array([[0.]])
y1, x1 = sim.intg(x1, u1, A1, B1, C1, D1)

u2 = y1
y2, x2 = sim.intg(x2, u2, A2, B2, C2, D2)

for t in sim.T:
	
	inpt = conn.recv(4096)
	start_time = time.time()
	if not inpt: break

	u1 = pickle.loads(inpt) 
	y1, x1 = sim.intg(x1, u1, A1, B1, C1, D1)

	u2 = y1
	y2, x2 = sim.intg(x2, u2, A2, B2, C2, D2)

	data = np.array([u1[0], y2[0]]) 
	# print data
	data = pickle.dumps(data)
	conn.send(data)
	z.send(data) 

	Y.extend([y2])
	U.extend([u1])
	T.extend([t])

	m = z.recv()

	print t

	# print time.time() - start_time

	i = i + 1

Y = np.array(Y)
Y.resize(Y.shape[0])
U = np.array(U)
U.resize(U.shape[0])
T = np.array(T)
T.resize(T.shape[0])

# show plots
sim.timePlot(T, Y, U, 'splt') # time domain plots 
sim.freqPlot(Y, U) # frequency domain plots 
plt.show()