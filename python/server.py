import time
import zmq
import pickle

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

while True:
    #  Wait for next request from client
    data = socket.recv()
    # print("Received request: %s" % message)
    data = pickle.loads(data)

    print data

    #  Do some 'work'
    # time.sleep(1)

    #  Send reply back to client
    socket.send(b"World")