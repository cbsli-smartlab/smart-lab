import control
import numpy as np
import scipy as sci
import math
from math import sqrt
from control.matlab import *
import sys

def lowlevelexperiment1(C1,C2,y):
	x = 0
	if y == 0:
		if (C1 < 0.5 or C1 > 1) or (C2 < 0.5 or C2 > 1):
			x = 1
			print "current out of bounds"
			return x
		if abs(C2-C1) < 0.3:
			x = 2 # error -> there is not enough of a difference between the input currents 
			print "not enough of a difference between the input currents "
			return x
		else:
			# run the experiment
			print "running experiment 1 three times"
			return x 
	else: # repeat experiement		
		  #if y == 1: # repeat experiment
		if (C1 < 0.5 or C1 > 1) or (C2 < 0.5 or C2 > 1):
			x = 1 # error -> current inputs are not within safe bounds
			print "current out of bounds"
			return x
		else:
			print "running experiment 1 - sequential/repeat"	
			return x 


def lowlevelexperiment1_freeplay(C1):
	x = 0
	if (C1 < 0.5 or C1 > 1):
		x = 1 # error -> current inputs are not within safe bounds
		print "current out of bounds"
		return x
	else:
		print "running experiment 1 - free play"	
		return x

def lowlevelexperiment2(kp1,kp2, kp3, y):
	x = 0
	A = np.array = [kp1,kp2,kp3]	
	if y == 0:
		if (kp1 < 0 or kp1 > 0.7) or (kp2 < 0 or kp2 > 0.7) or (kp3 < 0 or kp3 > 0.7):
			x = 8 # error -> kp values are not within safe bounds
			print "kp values not in safe limits"
			return x
		else: 
			i = 0
			while i < 2:
				j = i+1
				while j < 3:
					k = abs(A[i]-A[j])
					if k < 0.2:
						x = 6 # error -> there is not enough differnece between the three kp values
						print "insufficient difference between kp values"
						return x
					else: 
						j = j+1
				i = i+1
		if x == 0:
			print "running experiment 2 - sequential"	
			return x			
	else: # y == 1
		if (kp1 < 0 or kp1 > 0.7) or (kp2 < 0 or kp2 > 0.7) or (kp3 < 0 or kp3 > 0.7):
			x = 8 # error -> kp values are not within safe bounds
			print "kp values not in safe limits"
			return x
		if x == 0:
			print "running experiment 2 - sequential/repeat"	
			return x	

def lowlevelexperiment2_freeplay(kp, zd):
	x = 0
	if kp < 0 or kp > 0.7:
		x = 8 # error -> kp values are not within safe bounds
		print "kp values not in safe bounds"
		return x
	if zd < 0.5 or zd > 1.2:
		x = 4 # error -> input damping ratio is not within bounds
		print "damping ratio not in safe bounds "
		return x
	if x == 0:
		pm = stabilitycheck(kp, zd, 0.00114)
		if pm < 25: 
			x = 3 # error -> phase margin is too low
			print "phase margin is too low"
			return x
	if x == 0:
		print "running experiment 2 free play"
		return x

def lowlevelexperiment4(da_1, da_2, da_3, y):
	x = 0
	A = np.array = [da_1,da_2,da_3]	
	if y == 0:
		if da_1 > 120 or da_2 > 120 or da_3 > 120:
			x = 5 # error -> arm angle limitation exceeded
			print "arm angle limitation exceeded"
			return x
		else: 			
			i = 0
			while i < 2:
				j = i+1
				while j < 3:
					k = abs(A[i]-A[j])
					if k < 10:
						x = 7 # error -> there is not enough differnece between the three desired angle values
						print "insufficient difference between angle values"
						return x
					else: 
						j = j+1
				i = i+1
		if x == 0: 
			# run the experiment 3 times
			print "running experiment 4 - sequential"	
			return x				

	else: # y = 1
		if da_1 > 120 or da_2 > 120 or da_3 > 120:
			x = 5 # error -> arm angle limitation exceeded
			print "arm angle limitation exceeded"
			return x
		if x == 0: 
			# run the experiment 3 times
			print "running experiment 4 - sequential/repeat"	
			return x	

def lowlevelexperiment4_freeplay(da_1, kp, fq, zd):
	x = 0
	if da_1 > 120:
		x = 5 # error -> arm limitation exceeded
		print "arm angle limitation exceeded"
		return x
	if kp < 0 or kp > 0.6:
		x = 9 # error -> kp value not within safe bounds
		print "kp value not within safe bound"
		return x
	if fq < 10 or fq > 40: 
		x = 11 # error -> input cutoff frequency is not within bounds
		print "cutoff frequency not within bound"
		return x		
	if zd < 0.5 or zd > 1.2:
		x = 10
		print "damping ratio not within bounds"
		return x
	if x == 0:
		pm = stabilitycheck(kp, zd, 0.00114)
		if pm < 25: 
			x = 3 # error -> phase margin is too low
			print "phase margin is too low"
		return x
	if x == 0:
		print "running experiment 2 free play"
		return x

def stabilitycheck(kp, zd, T): 

		# SYSTEM PARAMETERS
		N = 8377.5733
		kt = .0276
		h = .95
		k = 350000
		m = 360
		beff = 2200
		b = N*kt*h
		b1 = k*b*kp
		T = -T # time delay
 		# CONTROL PARAMETER
		kd = (2*zd*math.sqrt(m*k*(1+b*kp))-beff)/(k*b)
		b2 = (k*b*kd)

		# NUMERATOR OF TRANSFER FUNCTION
		a = b2/30240. * (T**5)
		c = b2/1008. * (T**4) + b1/30240. * (T**5)
		d = b2/72. * (T**3) + b1/1008. * (T**4)
		e = b2/9. * (T**2) + b1/72. * (T**3)
		f = b2/2. *T + b1/9. * (T**2)
		g = b2 + b1/2. * T
		h = b1
		num = np.array([a, c, d, e, f, g, h])
		# print num 

		# DENOMINATOR OF TRANSFER FUNCTION
		h = -(m * (T**5)) / 30240.
		i = (m * (T**4)) / 1008. - (beff * (T**5)) / 30240.
		j = -(m * (T**3)) / 72. + (beff * (T**4)) / 1008. - (k * (T**5)) / 30240.
		l = (m * (T**2)) / 9. - (beff * (T**3)) / 72. + (k * (T**4)) / 1008.
		n = -(m * T) / 2. + (beff * (T**2)) / 9. - (k * (T**3)) / 72.
		o = m - (beff * T) / 2. + (k * (T**2)) / 9.
		p = beff - (k * T) / 2. 
		q = k
		den = np.array([h,i,j,l,n,o,p,q])
		# print den 
 
		# CREATE TRANSFER FUNCTION
		sys_tf = control.tf(num,den)
		# print sys_tf

		# COMPUTE PHASE MARGIN
		gm, pm, Wcg, Wcp = margin(sys_tf) 
		print pm
		return pm


if __name__ == '__main__':
    r = float(sys.argv[1])
    a = float(sys.argv[2])
    b = float(sys.argv[3])
    c = float(sys.argv[4])
    d = float(sys.argv[5])
    if r == 1:
    	# experiment 1, sequential
		C1 = a 
		C2 = b
		y = c
		print "running system ID lab, sequential"    	
		lowlevelexperiment1(C1,C2,y)
    elif r == 2:
    	C1 = a
    	print "running system ID lab, free play"
    	lowlevelexperiment1_freeplay(C1)
    elif r == 3:
    	print "run system ID simulation"
    elif r == 4:
    	kp1 = a
    	kp2 = b
    	kp3 = c # FINISH THIS SECTION
    	y = d
    	print "running force control lab"
    	lowlevelexperiment2(kp1,kp2, kp3, y)
    elif r == 5:
    	kp = a
    	zd = b
    	print "running force control - free play lab"
    	lowlevelexperiment2_freeplay(kp,zd)
    elif r == 6:
    	da_1 = a
    	da_2 = b
    	da_3 = c 
    	y = d 
    	print "running position control lab" 
    	lowlevelexperiment4(da_1, da_2, da_3, y)
    elif r == 7: 
    	da_1 = a
    	kp = b 
    	fq = c
    	zd = d
    	print "running position control free play lab" 
    	lowlevelexperiment4_freeplay(da_1, kp, fq, zd);

    	
