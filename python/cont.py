from scipy import integrate 
from scipy import signal
import sympy as sym
import numpy as np
import matplotlib.pyplot as plt
import time

class cont(object):
	"""docstring for cont"""
	def __init__(self):

		self.tf = 2. # final time (s)
		self.n1 = 1000. # sampling frequency of simulation (Hz)
		self.n2 = 10. # sampling frequency of integration (Hz)
		self.fmin = .01 # minimum frequency of chirp signal (Hz)
		self.fmax = 100. # maximum frequency of chirp signal (Hz)
		self.T = np.linspace(0, self.tf, self.tf * self.n1 + 1)
	
	# system dynamics
	def sys(self, x, t, u, A, B):
		return np.dot(A, x) + np.dot(B, u)

	# controller dynamics
	def intg(self, x, u, A, B, C, D):
		y = np.dot(C, x) + np.dot(D, u)
		t2 = np.linspace(0., 1. / self.n1, self.n2 + 1)   
		x.resize(x.shape[0])
		u.resize(u.shape[0])
		sol = integrate.odeint(self.sys, x, t2, args=(u, A, B, ))	
		x = sol[int(self.n2)]
		x.resize(x.shape[0], 1)
		u.resize(u.shape[0], 1)
		# y = np.dot(C, x) + np.dot(D, u)
		return y, x

	def lti2sym(self, lsys, s=sym.Symbol('s'), symplify=True):
	    """ Convert Scipy's LTI instance to Sympy expression """	    
	    G = sym.Poly(lsys.num, s) / sym.Poly(lsys.den, s)
	    return sym.simplify(G) if symplify else G

	def sym2lti(self, xpr, s=sym.Symbol('s')):
	    """ Convert Sympy transfer function polynomial to Scipy LTI """
	    num, den = sym.simplify(xpr).as_numer_denom()  # expressions
	    p_num_den = sym.poly(num, s), sym.poly(den, s)  # polynomials
	    c_num_den = [sym.expand(p).all_coeffs() for p in p_num_den]  # coefficients
	    l_num, l_den = [sym.lambdify((), c)() for c in c_num_den]  # convert to floats
	    return signal.lti(l_num, l_den)

	def prop(self, z, p, k):
		num, den = signal.zpk2tf(z, p, k)
		
		if p.size is 0:
			r = np.zeros(0)
			p = np.zeros(0)
			k = num
		else:
			r, p, k = signal.residue(num, den)

		s = sym.Symbol('s')
		if p.size is 0:
			tf1 = 0.
		else:
			num1, den1 = signal.invres(r, p, np.array([0.]))
			if num1.size is 1 and num1[0] == 0.:
				tf1 = 0.
			else:
				tf1 = self.lti2sym(signal.lti(num1, den1), s)
		
		x = s / (s / (self.n1 / 10) + 1)
		tf2 = sym.Poly(k, x) / sym.Poly(np.array([1.]), s)
		tf3 = tf1 + tf2
		print tf3
		tf3 = self.sym2lti(tf3)
		z, p, k = signal.tf2zpk(tf3.num, tf3.den)
		# print z, p, k
		return z, p, k

	def zpk2ss(self, z, p, k):
		z, p, k = self.prop(z, p, k)
		if p.size is 0:
			z = np.array([0.])
			p = np.array([0.])
		A, B, C, D = signal.zpk2ss(z, p, k)
		D.reshape(D.shape[0], 1)
		return A, B, C, D

	def timePlot(self, T, Y, U):
		fig = plt.figure("Time Response")

		ax2=fig.add_subplot(2,1,1)
		plt.plot(T, U, linewidth = 2.0, label='current, $i$')
		ax2.set_title('Input',fontsize=20)
		ax2.set_ylabel(r"Motor Current (A)",fontsize=15)

		ax1 = fig.add_subplot(2,1,2)
		plt.plot(T, Y, linewidth = 2.0, label='spring force, $F_k$')
		ax1.set_title('Output',fontsize=20)
		ax1.set_ylabel(r"Spring Force (N)",fontsize=15)
		ax1.set_xlabel('Time (s)',fontsize=15)
		return

	def freqPlot(self, Y, U):
		fftY = np.fft.fft(Y)
		fftU = np.fft.fft(U)
		TF = fftY / fftU

		freq = np.linspace(self.fmin, self.n1, int(self.tf*self.n1)+1)
	
		end = int(np.shape((abs(TF)))[0] * (self.fmax/self.n1))
		dB = 20 * np.log10(abs(TF))
		dG = (np.unwrap(np.angle(TF)[:end]) / np.pi) * 180

		fig = plt.figure("Frequency Response")

		plt.subplot(211)
		plt.title("Magitude",fontsize=20)
		plt.plot(freq[:end], dB[:end])
		plt.ylabel(r"Magnitude (dB)",fontsize=15)

		plt.xscale('log')
		plt.subplot(212)
		plt.title("Phase",fontsize=20)
		plt.plot(freq[:end], dG[:end])
		plt.ylabel(r"Phase (degree)",fontsize=15)
		plt.xscale('log')
		plt.xlabel("Frequency (Hz)",fontsize=15)
		return
