from scipy import integrate 
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import time
import socket
import pickle
from cont import cont
from disc import disc

sim = disc()

HOST = 'localhost'    # The remote host
PORT = 50007              # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

U = signal.chirp(sim.T, sim.fmin, sim.tf, sim.fmax)

i = 0;

for t in sim.T:
	# start_time = time.time()
	u = np.array([[U[i]]])
	print u 
	inpt = pickle.dumps(u)
	s.send(inpt) 
	time.sleep(1/sim.n1)
	data = s.recv(4096) 

	# print time.time() - start_time
	i = i + 1


