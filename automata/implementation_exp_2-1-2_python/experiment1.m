function experiment1(gain) % input chirp signal maximum current
    % interface with Simulink
    load_system('Experiment1')
    % system parameter values, and correct formatting for input to Simulink
    ktN = 0.02662; % 0.0276*0.95 = kt*n
    numerator = num2str(2932149500*ktN); % multiply by N (8377.57) 
                                         % and k (350,000)  
    
    % format values for input to Simulink                                     
    numerator = ['[',numerator,']'];
    m = num2str(360);
    b = num2str(2200);  
    k = num2str(350000);
    string2 = ['[',m,' ',b,' ',k,']'];
    target = 10; % Hz, frequency at target time
    target = num2str(target);
    target = ['[',target,']']; 
    
    gain = num2str(gain); % current magnitude
    set_param('Experiment1/Gain', 'Gain', gain) 
    
    % input parameters to Simulink, including the "target" which is
    % frequency at target goals
    set_param('Experiment1/Transfer Fcn', 'Numerator', numerator) 
    set_param('Experiment1/Transfer Fcn', 'Denominator', string2)
    set_param('Experiment1/Chirp Signal', 'F2', target)

    % Input Chirp Signal in Simulink and Collect Output Force and Current
    % Data
    a = sim('Experiment1','SimulationMode','normal');
    b = a.get('simout'); % output force
    c = a.get('simout1'); % frequency
    assignin('base','b',b);


    % plot input current and output spring force
    figure
    subplot(2,1,1)
    plot(c)
    title('input current vs time') 
    xlabel('time (s)')
    ylabel('input current (A)')
    subplot(2,1,2)
    plot(b)
    title('output force vs time')
    xlabel('time (s)')
    ylabel('spring force (N)')
    
    % output data into arrays y and z
    y = b.Data; % output force
    z = c.Data; % current
    
    % Fast Fourier Transform
    fdout = fft(y); % output force
    fdin = fft(z); % frequency
    %divide for output/input transfer function
    h = fdout./fdin; % transfer function

    % compute frequency
    T = 0.001; % Sampling period (s)
    Fs = 1/T; % sampling frequency (samples/s)
    L = length(h); % length of signal

    f = Fs*(0.01:(L/2))/L; % accounting for the fact that fft gives symmetric 
                        % output

    % create Bode Plot from data
    figure
    
    % magnitude vs frequency
    h1 = 20*log10(abs(h));
    P1 = h1(1:L/2+1);
    P1(2:end-1) = P1(2:end-1);
    subplot(2,1,1)
    semilogx(f,P1) % magnitude (dB) 

    % phase vs frequency
    P2 = h(1:L/2+1);
    P2(2:end-1) = 2*P2(2:end-1);
    
    xlabel('Frequency (Hz)') 
    ylabel('Magnitude (dB)')
    subplot(2,1,2)
    semilogx(f,(180/pi)*angle(P2)) % phase (deg)
    xlabel('Frequency (Hz)') 
    ylabel('Phase (deg)')

end 