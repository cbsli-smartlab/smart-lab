function x = lowlevelexperiment1(C1,C2, y)
    % 0 for first experiment
    % 1 for repeat
    
    x = 0;
    if y == 0
        if (C1 < 0.5 || C1 > 1) || (C2 < 0.5 || C2 > 1)
            x = 1; % error -> current inputs are not within safe bounds 
        elseif abs(C2-C1) < 0.3
            x = 2; % error -> there is not enough of a difference between the 
                   % input currents 
        end
        
        if x == 0
            experiment1(C1);
            experiment1(C2);
        end
    else
        if (C1 < 0.5 || C1 > 1) || (C2 < 0.5 || C2 > 1)
            x = 1; % error -> current inputs are not within safe bounds
        end
       
        if x == 0 
            experiment1(C1);
            experiment1(C2);
        end 

    end 
end