function next_state = at_state2() 
global ExperimentID BodePlot_error
    get_current = 2;
    try
        prompt = 'please enter two enter_currents. (such as: [current1 current2])\n';
        two_currents = input(prompt);
        current1 = two_currents(1);
        current2 = two_currents(2);
        get_current = 1;
    catch
    end


    switch get_current
        case 1 
            disp('student entered currents'); 
            disp('system reaction: run_exp');
            BodePlot_error = lowlevelmain(ExperimentID, current1, current2, 0);
            next_state = 3; 
        otherwise
            disp('student selection: invalid inputs');
            next_state = 2;
    end
end