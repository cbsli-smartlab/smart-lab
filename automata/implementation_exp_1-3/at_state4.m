function next_state = at_state4() 
 
    prompt = 'Enter: 0: nothing; 1: select_exp;\n';
    x = input(prompt);

    switch x
        case 0 
            disp('student selection: 0. nothing input'); next_state = 1;
        case 1 
            disp('student selection: 1. select_exp'); 
            disp('system reaction: ask_accurent');
            next_state = 2; 
        otherwise
            disp('student selection: invalid inputs');
            next_state = 4;
    end
end