
global ExperimentID BodePlot_error
ExperimentID = 0;
BodePlot_error = -1;
current_state = 0;
next_state = 0;
i = 0;

while i <= 50
    
    i = i + 1;
    
    switch current_state
        case 0
            disp('############################');
            disp('initial_state');
            next_state = at_initial_state();
        case 1
            disp('############################');
            disp('state: 1');
            next_state = at_state1();
        case 2
            disp('###########################################');
            disp('state: 2');
            next_state = at_state2();
        case 3
            disp('###########################################');
            disp('state: 3');
            next_state = at_state3();
        case 4
            disp('###########################################');
            disp('state: 4');
            next_state = at_state4();
        case 5
            disp('###########################################');
            disp('state: 5');
            next_state = at_state5();
        otherwise
            disp('###########################################');
            disp('state: error');

    end
  
    current_state = next_state;    
    
end    