function next_state = at_state3() 
 
    global BodePlot_error

    switch BodePlot_error
        case 0 
            disp('llc: get_result_from_llc'); 
            disp('system reaction: show result');
            next_state = 4; 
        case 1 
            disp('llc: get_error_from_llc; % error -> current inputs are not within safe bounds (0.5 ~1.0)'); 
            disp('system reaction: ask current');
            next_state = 5;
        case 2
            disp('llc: get_error_from_llc; % error -> there is not enough of a difference between the % input currents'); 
            disp('system reaction: ask current');
            next_state = 5;
        otherwise
            disp('llc selection: invalid inputs');
            next_state = 3;
    end
end