function x = lowlevelexperiment4_freeplay(da_1,kp,fq,zd)
%input desired angle, kp, cutoff frequency, and damping ratio
    % input constraint: kp needs to be between 0 and 0.6
    % input constraint: z needs to be between 0.5 and 1.2
    % input constraint: cutoff frequency between 10-40 Hz
    format long 
    
    N=8377.5733;
    kt=0.0276;
    h=0.95;
    k=350000;
    m=360;
    ma=0.665;
    beff=2200;
    b=N*kt*h;
    kd=(2*zd*sqrt(m*k*(1+b*kp))-beff)/(k*b);
    J = .0164;
    B = .065;
    g = 9.81;
    l = .157; 
    
    x = 0;
    
        if da_1 > 120
            x = 5; % error -> arm angle limitation exceeded 
        end
        
        if (kp < 0 || kp > 0.6) 
            x = 9; % error -> kp values are not within safe bounds
        end 
        
        if (zd < 0.5 || zd > 1.2)
            x = 10; % error -> input damping ratio is not within bounds
        end 
        
        if (fq < 10 || fq > 40)
            x = 11; % error -> input cutoff frequency is not within bounds
        end 
        
        % open loop transfer function for force control
        sys = tf([k*b*kd (k*b*kp)],[m beff k],'OutputDelay', 0.00114);
        [~,Pm1,~,~] = margin(sys); 
        
        if Pm1 < 25
            x = 12; % error -> phase margin is too low
        end 
        
        if x == 0
            experiment4(da_1,fq,kd,kp)
        end

       
    

end 