#!/usr/bin/env python

from exp2_2_ctrl import ExampleCtrl

M = ExampleCtrl()
print('In order, the input variables: '+', '.join(M.input_vars))
print(M.move(reset=1, select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=1, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=1, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=1, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=1, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=1, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=1, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=1, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=1, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=1))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=1))
print(M.move(reset=0, select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0))

