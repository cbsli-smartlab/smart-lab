function experiment4(a_d,fq,kd,kp)
    
    % a_d is desired arm angle
    
    % interface with Simulink
    load_system('Experiment4')
       
%--------------------------------------------------------------------------
    % Transfer Function 1
    n1 = num2str(.0164);
    n2 = num2str(.065);
    n3 = num2str(0);
    d1 = num2str(1/((2^2)*(pi^2)*(fq^2))); 
    d2 = num2str(1.4142/(2*3.14*fq));
    d3 = num2str(1);

    numerator1 = ['[',n1,' ',n2,' ',n3,']'];
    denominator1 = ['[',d1,' ',d2,' ',d3,']'];
    
    
    set_param('Experiment4/Transfer Fcn', 'Numerator', numerator1) 
    set_param('Experiment4/Transfer Fcn', 'Denominator', denominator1)
%------------------------------------------------------------------------    
    % PID Controller
    
    Kp = num2str(kp);
    Kd = num2str(kd); 

    set_param('Experiment4/PID Controller1', 'P', Kp) 
    set_param('Experiment4/PID Controller1', 'D', Kd)    
%------------------------------------------------------------------------    
    % Transfer Function 3     
    d1 = num2str(0.0164); 
    d2 = num2str(.065);
    d3 = num2str(0);    
    
    denominator3 = ['[',d1,' ',d2,' ',d3,']'];
    set_param('Experiment4/Transfer Fcn3', 'Denominator', denominator3)     
    
%-------------------------------------------------------------------------    
    % format slope values for input to Simulink                                     
    slope = (a_d-60)/0.2;
    
    slope1 = slope;
    slope1 = num2str(slope1);
    slope1 = ['[',slope1,']']; 
    
    
    slope2 = -slope;
    slope2 = num2str(slope2);
    slope2 = ['[',slope2,']'];

    slope3 = -slope;
    slope3 = num2str(slope3);
    slope3 = ['[',slope3,']'];
    
    slope4 = slope;
    slope4 = num2str(slope4);
    slope4 = ['[',slope4,']'];

    % input parameters to Simulink, including the "target" which is
    % frequency at target goals
    set_param('Experiment4/Ramp', 'Slope', slope1) 
    set_param('Experiment4/Ramp1', 'Slope', slope2)
    set_param('Experiment4/Ramp2', 'Slope', slope3)
    set_param('Experiment4/Ramp3', 'Slope', slope4)
    
    % Reference Force in Simulink and Collect Output Force Data
    a = sim('Experiment4','SimulationMode','normal');
    bb = a.get('simout'); % output position
    
    figure
    plot(bb)
    xlabel('time')
    ylabel('arm angle')
end 