function x = lowlevelexperiment4(da_1,da_2,da_3,y)
    % 0 for first experiment
    % 1 for repeat
    A = [da_1 da_2 da_3]; 
    x = 0;

    kp = 0.05;
    N=8377.5733;
    kt=0.0276;
    h=0.95;
    k=350000;
    m=360;
    beff=2200;
    b=N*kt*h;
    zd=0.9;
    kd=(2*zd*sqrt(m*k*(1+b*kp))-beff)/(k*b);
    % cutoff frequency is fixed for this experiment
    fq = 10; % Hz
    
    if y == 0 
        if da_1 > 120 || da_2 > 120 || da_3 > 120
            x = 5; % error -> arm angle limitation exceeded 

        elseif da_1 <= 120 && da_2 <= 120 && da_3 <= 120
            for i = 1:2
                for j = i+1:3
                    if abs(A(i)-A(j)) < 10
                        x = 7; % error -> there is not enough differnece between 
                               % the three desired anlge values            
                    end
                end

            end
        end
        if x == 0
           experiment4(da_1,fq,kd,kp)
           experiment4(da_2,fq,kd,kp)
           experiment4(da_3,fq,kd,kp)
        end
%-------------------------------------------------------------------------
% repeat experiment
    else    
        if da_1 > 120 || da_2 > 120 || da_3 > 120
            x = 5; % error -> arm angle limitation exceeded 
        end
        
        if x == 0
           experiment4(da_1,fq,kd,kp)
           experiment4(da_2,fq,kd,kp)
           experiment4(da_3,fq,kd,kp)
        end        
        
        
    end
    

end 
