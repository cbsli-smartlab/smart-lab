function x = lowlevelexperiment2_freeplay(kp, zd) 
        % constraint: kp needs to be between 0 and 0.7
        % constraint: z needs to be between 0.5 and 1.2
        
        N=8377.5733;
        kt=0.0276;
        h=0.95;
        k=350000;
        m=360;
        beff=2200;
        b=N*kt*h;
        kd=(2*zd*sqrt(m*k*(1+b*kp))-beff)/(k*b);
        
        x = 0; 

        
        if (kp < 0 || kp > 0.7) 
            x = 8; % error -> kp values are not within safe bounds
        end
        
        if (zd < 0.5 || zd > 1.2)
            x = 4; % error -> input damping ratio is not within bounds
        end 
        
        % open loop transfer function
        sys = tf([k*b*kd (k*b*kp)],[m beff k],'OutputDelay', 0.00114);
        [~,Pm,~,~] = margin(sys);        
        if Pm < 25
            x = 3; % error -> phase margin is too low
        end 
        % once all checks are performed, run experiment     
        if x == 0 
            experiment2(kp,zd) 
        end 
end 