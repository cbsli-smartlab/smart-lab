#!/usr/bin/env python

from exp1_7_ctrl import ExampleCtrl

M = ExampleCtrl()
print('In order, the input variables: '+', '.join(M.input_vars))
print(M.move(select_exp=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0))
print(M.move(select_exp=0, enter_current=1, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0))
print(M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0))
print(M.move(select_exp=0, enter_current=1, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0))
print(M.move(select_exp=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0))
print(M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0))
print(M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0))
print(M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=1))
print(M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0))
print(M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=1))
print(M.move(select_exp=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0))

