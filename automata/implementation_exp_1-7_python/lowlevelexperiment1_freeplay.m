function x = lowlevelexperiment1_freeplay(C1)
    % 0 for first experiment
    % 1 for repeat
    
    x = 0;
    
        if (C1 < 0.5 || C1 > 1) || (C2 < 0.5 || C2 > 1)
            x = 1; % error -> current inputs are not within safe bounds
        end
       
        if x == 0 
            experiment1(C1);
        end 

     
end