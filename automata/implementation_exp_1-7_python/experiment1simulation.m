% The student can input the three system ID parameters and create a
% simulated Bode Plot
function experiment1simulation(kth,m,beff)


    N=8377.5733;
    k=350000;
    
    %Correct values are
    %m=360;
    %beff=2200;
    %kt=0.0276;
    %h=0.95;
    % kt*h = 0.02622;


    sys = tf([N*kth*k],[m beff k]);
   
    figure
    opts = bodeoptions;
    opts.FreqUnits = 'Hz';
    h = bodeplot(sys,opts);


end 