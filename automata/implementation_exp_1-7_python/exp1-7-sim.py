#!/usr/bin/env python

from exp1_7_ctrl import ExampleCtrl
import matlab.engine
eng = matlab.engine.start_matlab()

error = -1
ExperimentID = 0

M = ExampleCtrl()

print("########################################")
var2 = raw_input("0. nothing input; 1. select experiment; 2. select simulation\n")
if int(var2) == 0:
    output = M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0)
if int(var2) == 1:
    ExperimentID = 1
    output = M.move(select_exp=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0)

if int(var2) == 2:
    ExperimentID = 3
    output = M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0)

i = 0




while i < 50 :
    i = i + 1
    
    if output["ask_current"] == 0 and output["run_exp"] == 0 and output["show_result"] == 0 and output["run_sim"] == 0 and output["ask_parameter"] == 0:
        print("########################################")
        var2 = raw_input("0. nothing input; 1. select experiment; 2. select simulation\n")
        
        if int(var2) == 0:
            output = M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0)
        if int(var2) == 1:
            ExperimentID = 1
            output = M.move(select_exp=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0)
        if int(var2) == 2:
            ExperimentID = 3
            output = M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0)

    if output["ask_current"] == 1:
        
        if error == 1.0:
            print("########################################")
            print("Get_error_from_llc: current inputs are not within safe bounds (0.5 ~1.0)\n")
        elif error == 2.0:
            print("########################################")
            print("Get_error_from_llc: there is not enough of a difference between the % input currents\n")
    
        print("########################################")
        curr1, curr2 = raw_input("Enter put two currents\n").split()
        curr1 = float(curr1)
        curr2 = float(curr2)
        output = M.move(select_exp=0, enter_current=1, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0)

    if output["run_exp"] == 1:
        error = eng.lowlevelmain(ExperimentID, curr1, curr2, 0, 0)
        raw_input("press enter to continue\n")

        if error == 0.0:
            output = M.move(select_exp=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0)
        else:
            output = M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0)

    if output["show_result"] == 1:
        print("########################################")
        var2 = raw_input("0. nothing input; 1. select experiment; 2.select simulation\n")
        if int(var2) == 0:
            output = M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0)
        if int(var2) == 1:
            ExperimentID = 1
            output = M.move(select_exp=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0)
        if int(var2) == 2:
            ExperimentID = 3
            output = M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0)

    if output["run_sim"] == 1:
        eng.lowlevelmain(ExperimentID, para1, para2, para3, 0)
        error = 0
        raw_input("press enter to continue\n")
        if error == 0.0:
            output = M.move(select_exp=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0)
        else:
            output = M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0)

    if output["ask_parameter"] == 1:
        if error == 3.0:
            print("########################################")
            print("Get_error_from_llc:  we do not have errors for simulation...\n")
        
        print("########################################")
        para1, para2, para3 = raw_input("Enter three parameters: kth,m,beff\n").split()
        para1 = float(para1)
        para2 = float(para2)
        para3 = float(para3)
        output = M.move(select_exp=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=1)
