#!/usr/bin/env python

from exp1_5_ctrl import ExampleCtrl

M = ExampleCtrl()
print('In order, the input variables: '+', '.join(M.input_vars))
print(M.move(select_exp=1, exp=1, enter_current=0, current=0, 
			enter_exp_data=0, exp_data=0, enter_exp_error=0, exp_error=0,
	        select_sim=0, sim=0, enter_parameter=0, parameter=0, 
	        enter_sim_data=0, sim_data=0, enter_sim_error=0, sim_error=0))
print(M.move(select_exp=0, exp=1, enter_current=1, current=1, 
			enter_exp_data=0, exp_data=0, enter_exp_error=0, exp_error=0,
	        select_sim=0, sim=0, enter_parameter=0, parameter=0, 
	        enter_sim_data=0, sim_data=0, enter_sim_error=0, sim_error=0))
print(M.move(select_exp=0, exp=1, enter_current=0, current=0, 
			enter_exp_data=0, exp_data=0, enter_exp_error=1, exp_error=1,
	        select_sim=0, sim=0, enter_parameter=0, parameter=0, 
	        enter_sim_data=0, sim_data=0, enter_sim_error=0, sim_error=0))
print(M.move(select_exp=0, exp=1, enter_current=1, current=1, 
			enter_exp_data=0, exp_data=0, enter_exp_error=0, exp_error=0,
	        select_sim=0, sim=0, enter_parameter=0, parameter=0, 
	        enter_sim_data=0, sim_data=0, enter_sim_error=0, sim_error=0))
print(M.move(select_exp=0, exp=1, enter_current=0, current=1, 
			enter_exp_data=1, exp_data=1, enter_exp_error=0, exp_error=0,
	        select_sim=0, sim=0, enter_parameter=0, parameter=0, 
	        enter_sim_data=0, sim_data=0, enter_sim_error=0, sim_error=0))
print(M.move(select_exp=0, exp=0, enter_current=0, current=1, 
			enter_exp_data=0, exp_data=1, enter_exp_error=0, exp_error=0,
	        select_sim=1, sim=1, enter_parameter=0, parameter=0, 
	        enter_sim_data=0, sim_data=0, enter_sim_error=0, sim_error=0))


