#!/usr/bin/env python

from exp1_3_ctrl import ExampleCtrl
import matlab.engine
eng = matlab.engine.start_matlab()

error = -1
ExperimentID = 0

M = ExampleCtrl()

print("########################################")
var2 = raw_input("0. nothing input; 1. select experiment\n")
if int(var2) == 0:
    output = M.move(select_exp=0, enter_current=0, enter_data=0, error=0)
if int(var2) == 1:
    ExperimentID = 1
    output = M.move(select_exp=1, enter_current=0, enter_data=0, error=0)

i = 0

while i < 50 :
    i = i + 1
    
    if output["ask_current"] == 0 and output["run_exp"] == 0 and output["show_result"] == 0:
        print("########################################")
        var2 = raw_input("0. nothing input; 1. select experiment\n")
        
        if int(var2) == 0:
            output = M.move(select_exp=0, enter_current=0, enter_data=0, error=0)
        if int(var2) == 1:
            ExperimentID = 1
            output = M.move(select_exp=1, enter_current=0, enter_data=0, error=0)

    if output["ask_current"] == 1:
        
        if error == 1.0:
            print("########################################")
            print("Get_error_from_llc: current inputs are not within safe bounds (0.5 ~1.0)\n")
        elif error == 2.0:
            print("########################################")
            print("Get_error_from_llc: there is not enough of a difference between the % input currents\n")
    
        print("########################################")
        curr1, curr2 = raw_input("Enter put two currents\n").split()
        curr1 = float(curr1)
        curr2 = float(curr2)
        output = M.move(select_exp=0, enter_current=1, enter_data=0, error=0)

    if output["run_exp"] == 1:
        error = eng.lowlevelmain(ExperimentID, curr1, curr2, 0, 0)
        raw_input("press enter to continue\n")

        if error == 0.0:
            output = M.move(select_exp=0, enter_current=0, enter_data=1, error=0)
        else:
            output = M.move(select_exp=0, enter_current=0, enter_data=0, error=1)

    if output["show_result"] == 1:
        print("########################################")
        var = raw_input("0. nothing input; 1. select experiment\n")
        if int(var) == 0:
            output = M.move(select_exp=0, enter_current=0, enter_data=0, error=0)
        elif int(var) == 0:
            output = M.move(select_exp=1, enter_current=0, enter_data=0, error=0)
