#!/usr/bin/env python

from exp2_1_ctrl import ExampleCtrl
import matlab.engine
eng = matlab.engine.start_matlab()

error = -1
ExperimentID = 0

first_id = 1
first_force = 1
first_position = 1

M = ExampleCtrl()

print("start", M.state)

print("########################################1")
var2 = raw_input("0. nothing input; 1. select experiment; 2. select simulation; 3. select force; 4. select position; \n")
if int(var2) == 0:
    output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
if int(var2) == 1:
    ExperimentID = 1
    output = M.move(select_id=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
if int(var2) == 2:
    ExperimentID = 3
    output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)

if int(var2) == 3:
    ExperimentID = 4
    output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=1, enter_kp=0, select_position=0, enter_angle=0)

if int(var2) == 4:
    ExperimentID = 6
    output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=1, enter_angle=0)

print("var2_output", output)


i = 0

while i < 50 :
    i = i + 1
               
               
    if output["ask_current"] == 0 and output["ask_kp"] == 0 and output["ask_angle"] == 0 and output["run_position"] == 0 and output["run_force"] == 0 and output["run_id"] == 0 and output["show_result"] == 0 and output["run_sim"] == 0 and output["ask_parameter"] == 0:
        print("########################################2")
        var2 = raw_input("0. nothing input; 1. select experiment; 2. select simulation; 3. select force; 4. select position; \n")
        
        if int(var2) == 0:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)

        if int(var2) == 1:
            ExperimentID = 1
            output = M.move(select_id=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)

        if int(var2) == 2:
            ExperimentID = 3
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)

        if int(var2) == 3:
            ExperimentID = 4
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=1, enter_kp=0, select_position=0, enter_angle=0)

        if int(var2) == 4:
            ExperimentID = 6
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=1, enter_angle=0)


    if output["ask_current"] == 1:
        if error == 1.0:
            print("########################################3.1")
            print("Get_error_from_llc: current inputs are not within safe bounds (0.5 ~1.0)\n")
        elif error == 2.0:
            print("########################################3.2")
            print("Get_error_from_llc: there is not enough of a difference between the % input currents\n")
    
        print("########################################3.3")
        curr1, curr2 = raw_input("Enter put two currents\n").split()
        curr1 = float(curr1)
        curr2 = float(curr2)
        output = M.move(select_id=0, enter_current=1, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)


    if output["ask_parameter"] == 1:
        if error == 3.0:
            print("########################################4.1")
            print("Get_error_from_llc:  we do not have errors for simulation...\n")
        
        print("########################################4.2")
        para1, para2, para3 = raw_input("Enter three parameters: kth,m,beff\n").split()
        para1 = float(para1)
        para2 = float(para2)
        para3 = float(para3)
        output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=1, select_force=0, enter_kp=0, select_position=0, enter_angle=0)


    if output["ask_kp"] == 1:
    
        if error == 8.0:
            print("########################################5.1")
            print("Get_error_from_llc: kp values are not within safe bounds (0.0 ~0.7)\n")
        elif error == 6.0:
            print("########################################5.2")
            print("Get_error_from_llc: here is not enough differnece between the three kp values \n")

        print("########################################5.3")
        kp1, kp2, kp3 = raw_input("Enter put three kps\n").split()
        kp1 = float(kp1)
        kp2 = float(kp2)
        kp3 = float(kp3)
        output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=1, select_position=0, enter_angle=0)


    if output["ask_angle"] == 1:
    
        if error == 5.0:
            print("########################################6.1")
            print("Get_error_from_llc: arm angle limitation exceeded  (0.0 ~ 120.0)\n")
        elif error == 7.0:
            print("########################################6.2")
            print("Get_error_from_llc: here is not enough differnece between the three desired anlge values\n")
        
        print("########################################6.3")
        da1, da2, da3 = raw_input("Enter put three desired angle\n").split()
        da1 = float(da1)
        da2 = float(da2)
        da3 = float(da3)
        output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=1)


    if output["run_force"] == 1:
        
        if first_force == 1:
            error = eng.lowlevelmain(ExperimentID, kp1, kp2, kp3, 0)
        
        else:
            error = eng.lowlevelmain(ExperimentID, kp1, kp2, kp3, 1)
        
        print("########################################7")
        raw_input("press enter to continue\n")
        
        if error == 0.0:
            output = M.move(select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
            first_force = 0
        else:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)


    if output["run_position"] == 1:
        
        if first_position == 1:
            error = eng.lowlevelmain(ExperimentID, da1, da2, da3, 0)
        else:
            error = eng.lowlevelmain(ExperimentID, da1, da2, da3, 1)
        print("########################################8")
        raw_input("press enter to continue\n")
        
        if error == 0.0:
            output = M.move(select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
            first_position = 0
        else:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)


    if output["run_id"] == 1:
        
        if first_id == 1:
            error = eng.lowlevelmain(ExperimentID, curr1, curr2, 0, 0)
            print("first_id", first_id)
        else:
            print("first_id", first_id)
            error = eng.lowlevelmain(ExperimentID, curr1, curr2, 1, 0)

        print("########################################9")
        raw_input("press enter to continue\n")

        if error == 0.0:
            output = M.move(select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
            first_id = 0
        else:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)

    if output["run_sim"] == 1:
        eng.lowlevelmain(ExperimentID, para1, para2, para3, 0)
        error = 0
        print("########################################10")
        raw_input("press enter to continue\n")
        if error == 0.0:
            output = M.move(select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
        else:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
            


    if output["show_result"] == 1:
        print("########################################11")
        var2 = raw_input("0. nothing input; 1. select experiment; 2. select simulation; 3. select force; 4. select position; \n")
        if int(var2) == 0:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
        
        if int(var2) == 1:
            ExperimentID = 1
            output = M.move(select_id=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
        
        if int(var2) == 2:
            ExperimentID = 3
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
        
        if int(var2) == 3:
            ExperimentID = 4
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=1, enter_kp=0, select_position=0, enter_angle=0)
        
        if int(var2) == 4:
            ExperimentID = 6
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=1, enter_angle=0)
