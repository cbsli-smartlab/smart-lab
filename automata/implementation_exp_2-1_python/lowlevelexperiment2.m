function x = lowlevelexperiment2(kp1,kp2, kp3, y)
    % 0 for first experiment
    % 1 for repeat
    
    A = [kp1 kp2 kp3]; 
    
    x = 0; 
    if y == 0
        if (kp1 < 0 || kp1 > 0.7) || (kp2 < 0 || kp2 > 0.7) || (kp3 < 0 || kp3 > 0.7)
            x = 8; % error -> kp values are not within safe bounds

        elseif (kp1 >= 0 && kp1 <= 0.7) && (kp2 >= 0 && kp2 <= 0.7) && (kp3 >= 0 && kp3 <= 0.7)
            for i = 1:2
                for j = i+1:3
                    if abs(A(i)-A(j)) < 0.199999999999999
                        x = 6; % error -> there is not enough differnece between 
                               % the three kp values            
                    end
                end
            end
        end

        if x == 0
            experiment2(kp1,.9)
            experiment2(kp2,.9)
            experiment2(kp3,.9)
        else

        end
% ------------------------------------------------------------------------        
    else
        if (kp1 < 0 || kp1 > 0.7) || (kp2 < 0 || kp2 > 0.7) || (kp3 < 0 || kp3 > 0.7)
            x = 8; % error -> current inputs are not within safe bounds
        end

        if x == 0
            experiment2(kp1,.9)
            experiment2(kp2,.9)
            experiment2(kp3,.9)
        else

        end
        
        
    end
    
end
    
    
