#!/usr/bin/env python
from __future__ import print_function
import logging

from tulip import spec
from tulip import synth
from tulip.transys import machines


logging.basicConfig(level=logging.WARNING)


#
# Environment specification
#
# The environment can issue a park signal that the robot must respond
# to by moving to the lower left corner of the grid.  We assume that
# the park signal is turned off infinitely often.
#
env_vars = set()
env_init = set()                # empty set
env_safe = set()                # empty set
env_prog = set()                # []<>(!park)

# student inputs
env_vars |= {'select_exp', 'enter_current'}

# machine inputs
env_vars |= {'enter_data', 'enter_error'}

# initial assumption 
# env_init |= {'select_exp'} 

# mutual exclusion (student)
# mutual exclusion (machine)
env_safe |= {'enter_error -> !(select_exp || enter_data || enter_current)'}
env_safe |= {'select_exp -> !(enter_error || enter_data || enter_current)'}
env_safe |= {'enter_data -> !(select_exp || enter_error || enter_current)'}
env_safe |= {'enter_current -> !(select_exp || enter_data || enter_error)'}

# consistency (student)
# consistency (machine)
env_safe |= {'!(ask_current) -> X !(enter_current)'}
env_safe |= {'(run_exp) <-> X (enter_error || enter_data)'}

# liveness (student)
# liveness (machine)
env_prog |= {'select_exp'}
env_prog |= {'enter_current'}
env_prog |= {'enter_data'}

#
# System dynamics
#
# The system specification describes how the system is allowed to move
# and what the system is required to do in response to an environmental
# action.
#
sys_vars = set()
sys_init = set()
sys_safe = set()
sys_prog = set()                # empty set

# output to student
# output to machine
sys_vars |= {'ask_current', 'show_result'}
sys_vars |= {'run_exp'}

sys_vars |= {'exp', 'current', 'error', 'data'}

#
# System specification
#
# The system specification is that the robot should repeatedly revisit
# the upper right corner of the grid while at the same time responding
# to the park signal by visiting the lower left corner.  The LTL
# specification is given by
#
#     []<> X5 && [](park -> <>X0)
#
# Since this specification is not in GR(1) form, we introduce an
# environment variable X0reach that is initialized to True and the
# specification [](park -> <>X0) becomes
#
#     [](X (X0reach) <-> X0 || (X0reach && !park))
#

# Augment the system description to make it GR(1)

# deactivation (student)
# deactivation (machine)


# precondition (student)
# precondition (machine)
sys_safe |= {'(exp && !current) <-> ask_current'}
sys_safe |= {'(exp && data) <-> show_result'}
sys_safe |= {'(exp && current && !data) <-> run_exp'}

sys_safe |= {'((exp && !show_result) || X (select_exp)) <-> X (exp)'}
sys_safe |= {'((current && X (!enter_error)) || X (enter_current)) <-> X (current)'}
sys_safe |= {'((error && X (!enter_current)) || X (enter_error)) <-> X (error)'}
sys_safe |= {'((data && X (!select_exp)) || X (enter_data)) <-> X (data)'}

# liveness
sys_prog |= {'show_result'}

# Create a GR(1) specification
specs = spec.GRSpec(env_vars, sys_vars, env_init, sys_init,
                    env_safe, sys_safe, env_prog, sys_prog)

#
# Controller synthesis
#
# The controller decides based on current variable values only,
# without knowing yet the next values that environment variables take.
# A controller with this information flow is known as Moore.
specs.moore = True
# Ask the synthesizer to find initial values for system variables
# that, for each initial values that environment variables can
# take and satisfy `env_init`, the initial state satisfies
# `env_init /\ sys_init`.
specs.qinit = '\E \A'  # i.e., "there exist sys_vars: forall sys_vars"

# At this point we can synthesize the controller
# using one of the available methods.
strategy = synth.synthesize('gr1c', specs)
assert strategy is not None, 'unrealizable'

# Generate a graphical representation of the controller for viewing,
# or a textual representation if pydot is missing.
if not strategy.save('exp1-4.png'):
    print(strategy)

# # simulate
print(strategy)
machines.random_run(strategy, N=10)

from tulip import dumpsmach
dumpsmach.write_python_case("exp1_4_ctrl.py", strategy, classname="ExampleCtrl")