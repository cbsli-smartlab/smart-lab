#!/usr/bin/env python
from __future__ import print_function
import logging

from tulip import spec
from tulip import synth
from tulip.transys import machines


logging.basicConfig(level=logging.WARNING)


#
# Environment specification
#
# The environment can issue a park signal that the robot must respond
# to by moving to the lower left corner of the grid.  We assume that
# the park signal is turned off infinitely often.
#
env_vars = set()
env_init = set()                # empty set
env_safe = set()                # empty set
env_prog = set()                # []<>(!park)

# student inputs
env_vars |= {'select_exp', 'enter_current'}

# machine inputs
env_vars |= {'enter_data', 'error'}

# initial assumption 
env_init |= {'select_exp'} 

# mutual exclusion (student)
# env_safe |= {'X (select_exp) -> X (!select_sim)'}
# env_safe |= {'X (select_sim) -> X (!select_exp)'}
# env_safe |= {'X (enter_current) -> X (!enter_parameter)'}
# env_safe |= {'X (enter_parameter) -> X (!enter_current)'}

# mutual exclusion (machine)
# env_safe |= {'X (enter_data) -> X (!enter_sim && !error)'}
# env_safe |= {'X (enter_sim) -> X (!enter_data && !error)'}
# env_safe |= {'X (error) -> X (!enter_data && !enter_sim)'}
# env_safe |= {'X (error) -> X (!enter_data)'}
# env_safe |= {'X (enter_data) -> X (!error)'}
env_safe |= {'(select_exp || enter_data || enter_current) -> !error'}
env_safe |= {'error -> !(select_exp || enter_data || enter_current)'}
env_safe |= {'(error || enter_data || enter_current) -> !select_exp'}
env_safe |= {'select_exp -> !(error || enter_data || enter_current)'}
env_safe |= {'(select_exp || error || enter_current) -> !enter_data'}
env_safe |= {'enter_data -> !(select_exp || error || enter_current)'}
env_safe |= {'(select_exp || enter_data || error) -> !enter_current'}
env_safe |= {'enter_current -> !(select_exp || enter_data || error)'}

# consistency (student)
# env_safe |= {'(select_exp && !show_result) -> X (select_exp)'}
# env_safe |= {'(!select_sim && !show_result -> X (!select_sim))'}
# env_safe |= {'(select_sim && !show_result -> select_sim)'}
# env_safe |= {'(!enter_current && !ask_current) -> X (!enter_current)'}
# env_safe |= {'(enter_current && !ask_current) -> X (enter_current)'}
# env_safe |= {'(!enter_parameter && !ask_parameter) -> X (!enter_parameter)'}
# env_safe |= {'(enter_parameter && !ask_parameter) -> X (enter_parameter)'}
env_safe |= {'(select_exp && ask_current) -> X (!select_exp)'}
env_safe |= {'(error && ask_current) -> X (!error)'}
env_safe |= {'!(ask_current) -> X !(enter_current)'}
env_safe |= {'(ask_current) -> X (enter_current)'}
env_safe |= {'!(enter_current || run_exp) -> X !(error || enter_data)'}
env_safe |= {'(enter_current && run_exp) -> X (error || enter_data)'}

# consistency (machine)
# env_safe |= {'(!enter_data && !run_exp) -> X (!enter_data)'}
# env_safe |= {'(enter_data && !run_exp) -> X (enter_data)'}
# env_safe |= {'(!enter_sim && !run_sim) -> X (!enter_sim)'}
# env_safe |= {'(enter_sim && !run_sim) -> X (enter_sim)'}
# env_safe |= {'(!error && !(run_exp || run_sim)) -> X (!error)'}
# env_safe |= {'(error && !(run_exp || run_sim)) -> X (error)'}
# env_safe |= {'(!error && !run_exp) -> X (!error)'}
# env_safe |= {'(error && !run_exp) -> X (error)'}
# env_safe |= {'(!error && !run_exp) -> X (!error)'}
# env_safe |= {'(error && !run_exp) -> X (error)'}

# liveness (student)
# env_safe |= {'(ask_current && X (enter_current)) || !ask_current'}
# env_prog |= {'(ask_parameter && X (enter_parameter)) || !ask_parameter'}
env_prog |= {'select_exp'}
env_prog |= {'enter_data'}
# env_prog |= {'error'}
# env_prog |= {'select_sim'}

# liveness (machine)
# env_safe |= {'(run_exp && (X (enter_data || error))) || !run_exp'}
# env_prog |= {'(run_sim && (X (enter_sim) || X (error))) || !run_sim'}

#
# System dynamics
#
# The system specification describes how the system is allowed to move
# and what the system is required to do in response to an environmental
# action.
#
sys_vars = set()
sys_init = set()
sys_safe = set()
sys_prog = set()                # empty set

# output to student
# sys_vars |= {'ask_current', 'ask_parameter', 'show_result'}
sys_vars |= {'ask_current', 'show_result'}

# output to machine
# sys_vars |= {'run_exp', 'run_sim'}
sys_vars |= {'run_exp'}

sys_init |= {'ask_current'}

#
# System specification
#
# The system specification is that the robot should repeatedly revisit
# the upper right corner of the grid while at the same time responding
# to the park signal by visiting the lower left corner.  The LTL
# specification is given by
#
#     []<> X5 && [](park -> <>X0)
#
# Since this specification is not in GR(1) form, we introduce an
# environment variable X0reach that is initialized to True and the
# specification [](park -> <>X0) becomes
#
#     [](X (X0reach) <-> X0 || (X0reach && !park))
#

# Augment the system description to make it GR(1)

# deactivation (student)
# sys_safe |= {'(ask_current && X (enter_current)) -> X (!ask_current)'}
# sys_safe |= {'(ask_parameter && X (enter_parameter)) -> X (!ask_parameter)'}
# sys_safe |= {'show_result -> X (!show_result)'}

# deactivation (machine)
# sys_safe |= {'(run_exp && (X (enter_data || error))) -> X (!run_exp)'}
# sys_safe |= {'(run_sim && X (enter_sim || error)) -> X (!run_sim)'}

# precondition (student)
sys_safe |= {'!(select_exp || error) -> !ask_current'}
sys_safe |= {'(select_exp || error) -> ask_current'}
# sys_safe |= {'!select_sim -> !ask_parameter'}
# sys_safe |= {'(!enter_data && !enter_sim) -> !show_result'}
# sys_safe |= {'!enter_current -> !show_result'}
sys_safe |= {'!enter_data -> !show_result'}
sys_safe |= {'enter_data -> show_result'}
# sys_safe |= {'!select_exp -> !show_result'}

# precondition (machine)
sys_safe |= {'!enter_current -> !run_exp'}
sys_safe |= {'enter_current -> run_exp'}
# sys_safe |= {'!enter_parameter -> !run_sim'}

# liveness
sys_prog |= {'show_result'}
# sys_prog |= {'error -> ask_current'}
# sys_prog |= {'run_exp'}

# Create a GR(1) specification
specs = spec.GRSpec(env_vars, sys_vars, env_init, sys_init,
                    env_safe, sys_safe, env_prog, sys_prog)

#
# Controller synthesis
#
# The controller decides based on current variable values only,
# without knowing yet the next values that environment variables take.
# A controller with this information flow is known as Moore.
specs.moore = True
# Ask the synthesizer to find initial values for system variables
# that, for each initial values that environment variables can
# take and satisfy `env_init`, the initial state satisfies
# `env_init /\ sys_init`.
specs.qinit = '\E \A'  # i.e., "there exist sys_vars: forall sys_vars"

# At this point we can synthesize the controller
# using one of the available methods.
strategy = synth.synthesize('gr1c', specs)
assert strategy is not None, 'unrealizable'

# Generate a graphical representation of the controller for viewing,
# or a textual representation if pydot is missing.
# if not strategy.save('exp1-3.png'):
print(strategy)

# simulate
# print(strategy)
# machines.random_run(strategy, N=10)

# from tulip import dumpsmach
# dumpsmach.write_python_case("exp1_3_ctrl.py", strategy, classname="ExampleCtrl")

# from exp1_3_ctrl import ExampleCtrl

# M = ExampleCtrl()
# print('In order, the input variables: '+', '.join(M.input_vars))
# print(M.move(select_exp=1, enter_current=0, enter_data=0, error=0))
# print(M.move(select_exp=0, enter_current=1, enter_data=0, error=0))
# print(M.move(select_exp=0, enter_current=0, enter_data=1, error=0))
# for i in xrange(10):
#     input_values = {"park": 0}
#     print(M.move(**input_values))