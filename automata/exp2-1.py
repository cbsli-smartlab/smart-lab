#!/usr/bin/env python
from __future__ import print_function
import logging

from tulip import spec
from tulip import synth
from tulip.transys import machines

logging.basicConfig(level=logging.WARNING)

#
# Environment specification
#
# The environment can issue a park signal that the robot must respond
# to by moving to the lower left corner of the grid.  We assume that
# the park signal is turned off infinitely often.
#
env_vars = set()
env_init = set()                # empty set
env_safe = set()                # empty set
env_prog = set()                # []<>(!park)

# student inputs
# machine inputs
env_vars |= {'select_id', 'enter_current'}
env_vars |= {'select_sim', 'enter_parameter'}
env_vars |= {'select_force', 'enter_kp'}
env_vars |= {'select_position', 'enter_angle'}
env_vars |= {'enter_data', 'enter_error'}

# mutual exclusion (student)
# mutual exclusion (machine)
env_safe |= {'enter_error -> !(select_id || enter_data || enter_current || select_sim || enter_parameter || select_force || enter_kp || select_position || enter_angle)'}
env_safe |= {'enter_data -> !(select_id || enter_error || enter_current || select_sim || enter_parameter || select_force || enter_kp || select_position || enter_angle)'}
env_safe |= {'select_id -> !(enter_error || enter_data || enter_current || select_sim || enter_parameter || select_force || enter_kp || select_position || enter_angle)'}
env_safe |= {'enter_current -> !(select_id || enter_data || enter_error || select_sim || enter_parameter || select_force || enter_kp || select_position || enter_angle)'}
env_safe |= {'select_sim -> !(enter_error || enter_data || enter_parameter || enter_current || select_id || select_force || enter_kp || select_position || enter_angle)'}
env_safe |= {'enter_parameter -> !(select_sim || enter_data || enter_error || select_id || enter_current || select_force || enter_kp || select_position || enter_angle)'}
env_safe |= {'select_force -> !(enter_error || enter_data || enter_parameter || enter_current || select_id || select_sim || enter_kp || select_position || enter_angle)'}
env_safe |= {'enter_kp -> !(select_sim || enter_data || enter_error || select_id || enter_current || select_force || enter_parameter || select_position || enter_angle)'}
env_safe |= {'select_position -> !(enter_error || enter_data || enter_parameter || enter_current || select_id || select_sim || enter_kp || select_force || enter_angle)'}
env_safe |= {'enter_angle -> !(select_sim || enter_data || enter_error || select_id || enter_current || select_force || enter_parameter || select_position || enter_kp)'}

# consistency (student)
# consistency (machine)
env_safe |= {'(ask_current) <-> X (enter_current)'}
env_safe |= {'(ask_parameter) <-> X (enter_parameter)'}
env_safe |= {'(ask_kp) <-> X (enter_kp)'}
env_safe |= {'(ask_angle) <-> X (enter_angle)'}
env_safe |= {'(run_id || run_sim || run_force || run_position) <-> X (enter_error || enter_data)'}

# liveness (student)
# liveness (machine)
env_prog |= {'select_id'}
env_prog |= {'select_sim'}
env_prog |= {'select_force'}
env_prog |= {'select_position'}
env_prog |= {'enter_data'}

#
# System dynamics
#
# The system specification describes how the system is allowed to move
# and what the system is required to do in response to an environmental
# action.
#
sys_vars = set()
sys_init = set()
sys_safe = set()
sys_prog = set()                # empty set

# output to student
# output to machine
sys_vars |= {'ask_current', 'ask_parameter', 'ask_kp', 'ask_angle', 'show_result'}
sys_vars |= {'run_id', 'run_sim', 'run_force', 'run_position'}
# sys_vars |= {'id', 'sim'}

#
# System specification
#
# The system specification is that the robot should repeatedly revisit
# the upper right corner of the grid while at the same time responding
# to the park signal by visiting the lower left corner.  The LTL
# specification is given by
#
#     []<> X5 && [](park -> <>X0)
#
# Since this specification is not in GR(1) form, we introduce an
# environment variable X0reach that is initialized to True and the
# specification [](park -> <>X0) becomes
#
#     [](X (X0reach) <-> X0 || (X0reach && !park))
#

# Augment the system description to make it GR(1)

# deactivation (student)
# sys_safe |= {'(ask_current && X (enter_current)) -> X (!ask_current)'}
# sys_safe |= {'(ask_parameter && X (enter_parameter)) -> X (!ask_parameter)'}
# sys_safe |= {'show_result -> X (!show_result)'}

# deactivation (machine)
# sys_safe |= {'(run_id && (X (enter_id_data || enter_id_error))) -> X (!run_id)'}
# sys_safe |= {'(run_sim && X (enter_sim || enter_id_error)) -> X (!run_sim)'}

# precondition (student)
# precondition (machine)
# sys_safe |= {'((run_id && X (enter_error)) || X (select_id)) <-> X (ask_current)'}
# sys_safe |= {'((run_sim && X (enter_error)) || X (select_sim)) <-> X (ask_parameter)'}
sys_safe |= {'select_force -> ask_kp'}
sys_safe |= {'(run_force && X (enter_error)) -> X (ask_kp)'}
sys_safe |= {'ask_kp -> (enter_error || select_force)'}
sys_safe |= {'select_id -> ask_current'}
sys_safe |= {'(run_id && X (enter_error)) -> X (ask_current)'}
sys_safe |= {'ask_current -> (enter_error || select_id)'}
sys_safe |= {'select_sim -> ask_parameter'}
sys_safe |= {'(run_sim && X (enter_error)) -> X (ask_parameter)'}
sys_safe |= {'ask_parameter -> (enter_error || select_sim)'}
sys_safe |= {'select_position -> ask_angle'}
sys_safe |= {'(run_position && X (enter_error)) -> X (ask_angle)'}
sys_safe |= {'ask_angle -> (enter_error || select_position)'}

sys_safe |= {'enter_current <-> run_id'}
sys_safe |= {'enter_parameter <-> run_sim'}
sys_safe |= {'enter_kp <-> run_force'}
sys_safe |= {'enter_angle <-> run_position'}
sys_safe |= {'enter_data <-> show_result'}

# sys_safe |= {'((id && !show_result) || X (select_id)) <-> X (id)'}
# sys_safe |= {'((sim && !show_result) || X (select_sim)) <-> X (sim)'}

# liveness

# Create a GR(1) specification
specs = spec.GRSpec(env_vars, sys_vars, env_init, sys_init,
                    env_safe, sys_safe, env_prog, sys_prog)

#
# Controller synthesis
#
# The controller decides based on current variable values only,
# without knowing yet the next values that environment variables take.
# A controller with this information flow is known as Moore.
specs.moore = True
# Ask the synthesizer to find initial values for system variables
# that, for each initial values that environment variables can
# take and satisfy `env_init`, the initial state satisfies
# `env_init /\ sys_init`.
specs.qinit = '\E \A'  # i.e., "there exist sys_vars: forall sys_vars"

# At this point we can synthesize the controller
# using one of the available methods.
strategy = synth.synthesize('gr1c', specs)
assert strategy is not None, 'unrealizable'

# Generate a graphical representation of the controller for viewing,
# or a textual representation if pydot is missing.
# if not strategy.save('exp1-3.png'):
#     print(strategy)

# simulate
print(strategy)
machines.random_run(strategy, N=10)

from tulip import dumpsmach
dumpsmach.write_python_case("exp2_1_ctrl.py", strategy, classname="ExampleCtrl")

# from id1_6_ctrl import ExampleCtrl

# M = ExampleCtrl()
# print('In order, the input variables: '+', '.join(M.input_vars))
# print(M.move(select_id=1, enter_current=0, enter_id_data=0, enter_id_error=0))
# print(M.move(select_id=0, enter_current=1, enter_id_data=0, enter_id_error=0))
# print(M.move(select_id=0, enter_current=0, enter_id_data=1, enter_id_error=0))
# for i in xrange(10):
#     input_values = {"park": 0}
#     print(M.move(**input_values))