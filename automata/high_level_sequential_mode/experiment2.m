% input kp gain

function experiment2(kp,zd)

    N=8377.5733;
    kt=0.0276;
    h=0.95;
    k=350000;
    m=360;
    beff=2200;
    b=N*kt*h; 
    Fr = 50; % reference force, N
    kd=(2*zd*sqrt(m*k*(1+b*kp))-beff)/(k*b);


        % interface with Simulink
        load_system('Experiment2')

        % system parameter values, and correct formatting for input to Simulink
        numerator1 = kd;
        n1 = num2str(numerator1);
        numerator2 = kp;
        n2 = num2str(numerator2);                                

        % format values for input to Simulink                                     
        numerator = ['[',n1,' ',n2,']'];
        %denominator = ['[',d1,' ',d2,' ',d3,']'];
        force_r = num2str(Fr);
        Fr = ['[',force_r,']']; 

        % input parameters to Simulink, including the "target" which is
        % frequency at target goals
        set_param('Experiment2/Transfer Fcn', 'Numerator', numerator) 
        %set_param('Experiment2/Transfer Fcn', 'Denominator', denominator)
        set_param('Experiment2/Constant', 'value', Fr)

        % Reference Force in Simulink and Collect Output Force Data
        a = sim('Experiment2','SimulationMode','normal');
        bb = a.get('simout2'); % output force
        c = a.get('simout3'); % reference force
        assignin('base','b',b);
            % plot input current and output spring force
        figure
        subplot(2,1,1)
        plot(c)
        title('reference force vs time') 
        xlabel('time (s)')
        ylabel('reference force (N)')
        subplot(2,1,2)
        plot(bb)
        title('output force vs time')
        xlabel('time (s)')
        ylabel('output force (N)')
        
        kd=(2*zd*sqrt(m*k*(1+b*kp))-beff)/(k*b);
        numpd=[k*b*kd k+k*b*kp];
        denpd=[m beff+k*b*kd k+k*b*kp];
        CLTF=tf(numpd,denpd,'OutputDelay',  0.00114);
        
        figure
        opts = bodeoptions;
        opts.FreqUnits = 'Hz';
        h = bodeplot(CLTF,opts);

        %qnum = [1];
        %qden = [1/(2*pi*fq)^2 1.4142/(2*pi*fq) 1]
        %Q = tf(qnum, qden)
        


end