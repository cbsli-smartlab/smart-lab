function x = lowlevelmain(r,a,b,c,d)
    x = -1;
    if r == 1
        % system ID lab
        C1 = a;
        C2 = b;
        y = c;
        x = lowlevelexperiment1(C1,C2,y);
    end
    if r == 2
        % system ID free play
        C1 = a;
        lowlevelexperiment1_freeplay(C1);
    end
    
    if r == 3
        % system ID simulation
        kth = a;
        m = b;
        beff = c; 
        experiment1simulation(kth,m,beff);
    end
    
    if r == 4
        % force control lab
        kp1 = a;
        kp2 = b;
        kp3 = c;
        y = d;
        x = lowlevelexperiment2(kp1,kp2,kp3,y);
    end
    
    if r == 5
        % experiment 2 free play
        kp = a;
        zd = b; 
        lowlevelexperiment2_freeplay(kp,zd);
    end
    
    if r == 6
        % experiment 4 
        da_1 = a;
        da_2 = b;
        da_3 = c; 
        y = d;
        x = lowlevelexperiment4(da_1,da_2,da_3,y);
    end

    if r ==7 
        % experiment 4 free play
        da_1 = a; 
        kp = b; 
        fq = c; 
        zd = d; 
        lowlevelexperiment4_freeplay(da_1,kp,fq,zd);

    end 
end 
