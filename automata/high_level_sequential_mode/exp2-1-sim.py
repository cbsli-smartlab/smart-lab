#!/usr/bin/env python

from exp2_1_ctrl import ExampleCtrl
import matlab.engine
eng = matlab.engine.start_matlab()

error = -1
ExperimentID = 0

first_id = 1
first_force = 1
first_position = 1

M = ExampleCtrl()

print("######################################################################")
print("Welcom to Smart Lab!\n")
print("Here are four experiments:\n1. System Identification (SID);\n2. System Simulation;\n3. Force Control;\n4. Position Control\n")
print("Enter the number and the start the corresponding experiment. Have fun!")
print("######################################################################")

var2 = raw_input("1. System Identification (SID);\n2. System Simulation;\n3. Force Control;\n4. Position Control;\n5.Exit\n")
if int(var2) == 0:
    output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
if int(var2) == 1:
    ExperimentID = 1
    output = M.move(select_id=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
if int(var2) == 2:
    ExperimentID = 3
    output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)

if int(var2) == 3:
    ExperimentID = 4
    output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=1, enter_kp=0, select_position=0, enter_angle=0)

if int(var2) == 4:
    ExperimentID = 6
    output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=1, enter_angle=0)

if int(var2) == 5:
    exit()


i = 0

while i < 50 :
    i = i + 1
               
               
    if output["ask_current"] == 0 and output["ask_kp"] == 0 and output["ask_angle"] == 0 and output["run_position"] == 0 and output["run_force"] == 0 and output["run_id"] == 0 and output["show_result"] == 0 and output["run_sim"] == 0 and output["ask_parameter"] == 0:
        
        var2 = raw_input("1. System Identification (SID);\n2. System Simulation;\n3. Force Control;\n4. Position Control;\n5.Exit\n")
        
        if int(var2) == 0:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)

        if int(var2) == 1:
            ExperimentID = 1
            output = M.move(select_id=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)

        if int(var2) == 2:
            ExperimentID = 3
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)

        if int(var2) == 3:
            ExperimentID = 4
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=1, enter_kp=0, select_position=0, enter_angle=0)

        if int(var2) == 4:
            ExperimentID = 6
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=1, enter_angle=0)

        if int(var2) == 5:
            exit()


    if output["ask_current"] == 1:
        if error == 1.0:
            print("Error 1: current inputs are not within safe bounds (0.5 ~1.0)\n")
        elif error == 2.0:
            print("Error 2: there is not enough of a difference between the input currents\n")
        
        if first_id == 1:
            print("##########################################################")
            print("This is your first run of system identification experiment.\nYou need to enter two currents satisfying the following two requirements:")
            print("1. the safe bounds of currents are 0.5 ~ 1.0")
            print("2. the currents should have enough difference.")
            print("##########################################################")
        else:
            print("########################################")
            print("System Identification Experiment.\nYou need to enter two currents satisfying the following requirements:")
            print("1. the safe bounds of currents are 0.5 ~ 1.0")
            print("########################################")
    
        curr1, curr2 = raw_input("Enter two currents. (use space to separate inputs)\n").split()
        curr1 = float(curr1)
        curr2 = float(curr2)
        output = M.move(select_id=0, enter_current=1, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)


    if output["ask_parameter"] == 1:
        if error == 3.0:
            print("########################################4.1")
            print("Get_error_from_llc:  we do not have errors for simulation...\n")

        print("#############################")
        print("System Simulation Experiment.\nYou need to enter the system parameters:\n motor torque constant (k_th), effective motor inertia felt by the spring (m_k), effective motor-side damping felt by the spring (b_eff):")
        print("#############################")

        para1, para2, para3 = raw_input("Enter three parameters: kth,m,beff. (use space to separate inputs)\n").split()
        para1 = float(para1)
        para2 = float(para2)
        para3 = float(para3)
        output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=1, select_force=0, enter_kp=0, select_position=0, enter_angle=0)


    if output["ask_kp"] == 1:
    
        if error == 8.0:
            print("Error 8: kp values are not within safe bounds (0.0 ~0.7)\n")
        elif error == 6.0:
            print("Error 6: there is not enough differnece between the three kp values \n")
        
        
        if first_force == 1:
            print("##########################################################")
            print("This is your first run of force control experiment.\nYou need to enter three coefficients of proportional terms of PID controller that satisfy the following two requirements:")
            print("1. the safe bounds of Kps are 0 ~ 0.7")
            print("2. the kps should have enough difference.")
            print("##########################################################")
        else:
            print("########################################")
            print("Force Control Experiment.\nYou need to enter three coefficients of proportional terms of PID controller that satisfy the following requirements:")
            print("1. the safe bounds of Kps are 0 ~ 0.7")
            print("########################################")
        

        kp1, kp2, kp3 = raw_input("Enter three kps. (use space to separate inputs)\n").split()
        kp1 = float(kp1)
        kp2 = float(kp2)
        kp3 = float(kp3)
        output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=1, select_position=0, enter_angle=0)


    if output["ask_angle"] == 1:
    
        if error == 5.0:
            print("Error 5: arm angle limitation exceeded  (0.0 ~ 120.0)\n")
        elif error == 7.0:
            print("Error 7: there is not enough differnece between the three desired anlge values\n")
        
        if first_position == 1:
            print("######################################################")
            print("This is your first run of position control experiment.\nYou need to enter three desired angles that satisfy the following two requirements:")
            print("1. the safe bounds of desired angles are 0 ~ 120")
            print("2. the desired angles should have enough difference.")
            print("######################################################")
        else:
            print("############################")
            print("Position Control Experiment.\nYou need to enter three three desired angles that satisfy the following requirements:")
            print("1. the safe bounds of desired angles are 0 ~ 120")
            print("############################")

        da1, da2, da3 = raw_input("Enter three desired angle. (use space to separate inputs)\n").split()
        da1 = float(da1)
        da2 = float(da2)
        da3 = float(da3)
        output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=1)


    if output["run_force"] == 1:
        
        if first_force == 1:
            error = eng.lowlevelmain(ExperimentID, kp1, kp2, kp3, 0)
        
        else:
            error = eng.lowlevelmain(ExperimentID, kp1, kp2, kp3, 1)
        
        raw_input("press enter to continue\n")
        
        if error == 0.0:
            output = M.move(select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
            first_force = 0
        else:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)


    if output["run_position"] == 1:
        
        if first_position == 1:
            error = eng.lowlevelmain(ExperimentID, da1, da2, da3, 0)
        else:
            error = eng.lowlevelmain(ExperimentID, da1, da2, da3, 1)

        raw_input("press enter to continue\n")
        
        if error == 0.0:
            output = M.move(select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
            first_position = 0
        else:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)


    if output["run_id"] == 1:
        
        if first_id == 1:
            error = eng.lowlevelmain(ExperimentID, curr1, curr2, 0, 0)
        else:
            error = eng.lowlevelmain(ExperimentID, curr1, curr2, 1, 0)

        raw_input("press enter to continue\n")

        if error == 0.0:
            output = M.move(select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
            first_id = 0
        else:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)

    if output["run_sim"] == 1:
        eng.lowlevelmain(ExperimentID, para1, para2, para3, 0)
        error = 0
        raw_input("press enter to continue\n")
        if error == 0.0:
            output = M.move(select_id=0, enter_current=0, enter_data=1, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
        else:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=1, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
            


    if output["show_result"] == 1:
        print("########################################")
        var2 = raw_input("1. System Identification (SID);\n2. System Simulation;\n3. Force Control;\n4. Position Control;\n5.Exit\n")
        if int(var2) == 0:
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
        
        if int(var2) == 1:
            ExperimentID = 1
            output = M.move(select_id=1, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
        
        if int(var2) == 2:
            ExperimentID = 3
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=1, enter_parameter=0, select_force=0, enter_kp=0, select_position=0, enter_angle=0)
        
        if int(var2) == 3:
            ExperimentID = 4
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=1, enter_kp=0, select_position=0, enter_angle=0)
        
        if int(var2) == 4:
            ExperimentID = 6
            output = M.move(select_id=0, enter_current=0, enter_data=0, enter_error=0, select_sim=0, enter_parameter=0, select_force=0, enter_kp=0, select_position=1, enter_angle=0)
        if int(var2) == 5:
            exit()
